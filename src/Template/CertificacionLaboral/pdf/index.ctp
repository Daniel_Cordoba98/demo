<?php
    $dt = new DateTime;
    $formatter = new IntlDateFormatter('es_ES', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
    $formatter->setPattern("dd 'de' MMMM 'de' y");
?>

<style>
    p {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12.0pt !important;
        margin: 20px 60px !important;
    }

</style>

<!-- ENCABEZADO -->
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<!-- //  -->

<body>

    <?php echo $this->Html->image('certificacion/encabezado.PNG', ['fullBase' => true, 'width' => '100%']) ?>
    <h3 style="text-align: center; font-family: Arial, Helvetica, sans-serif;">CERTIFICACIÓN</h3>

    <!-- SALARIO 100% - INACTIVO -->
    <?php if (empty(trim($persona->bono_anual)) && !$persona->estado): ?>

    <p style="text-align: justify;">
        <b>GLORIA STELLA VELOZA FLAUTERO</b> identificada con cédula de ciudadanía No.52.886.080 de Bogotá, en su
        calidad de
        Subgerente de Recursos Humanos de la compañía <b>EZENTIS COLOMBIA S.A.S.</b>, sociedad legalmente constituida e
        identificada con Nit. 900.196.414-1 certifica que el <b>Sr.(a)
            <?php echo $persona->nombre_completo ?></b> identificado(a) con cédula de ciudadanía <b>No.
            <?php echo $persona->identificacion ?></b>, laboró en la compañía mediante contrato a término <b>
            <?php echo $persona->tipo_contrato ?></b> desde el día <b>
            <?php echo date_format($persona->fecha_ingreso, 'd/m/Y') ?></b>, hasta el dia <b>
            <?php echo date_format($persona->fecha_retiro, 'd/m/Y') ?>
        </b>, desempeñandose como
        <b>
            <?php echo $persona->cargo ?></b> con una asignación básica mensual de
        <?php echo \NumeroALetras::convertir(round($persona->salario_total, 0)) ?> PESOS M/CTE.,
        ($
        <?php echo $this->Number->format(round($persona->salario_total, 0)) ?>).
    </p>

    <!-- SALARIO 100% - ACTIVO -->
    <?php elseif (empty(trim($persona->bono_anual)) && $persona->estado): ?>

    <p style="text-align: justify;">
        <b>GLORIA STELLA VELOZA FLAUTERO</b> identificada con cédula de ciudadanía No.52.886.080 de Bogotá, en su
        calidad de
        Subgerente de Recursos Humanos de la compañía <b>EZENTIS COLOMBIA S.A.S.</b>, sociedad legalmente constituida e
        identificada con Nit. 900.196.414-1 certifica que el <b>Sr.(a)
            <?php echo $persona->nombre_completo ?></b> identificado(a) con cédula de ciudadanía <b>No.
            <?php echo $persona->identificacion ?></b>, labora en la compañía mediante contrato a término <b>
            <?php echo $persona->tipo_contrato ?></b> desde el día <b>
            <?php echo date_format($persona->fecha_ingreso, 'd/m/Y') ?></b>,
        desempeñandose como
        <b>
            <?php echo $persona->cargo ?></b> con una asignación básica mensual de
        <?php echo \NumeroALetras::convertir(round($persona->salario_total, 0)) ?> PESOS
        M/CTE., ($
        <?php echo $this->Number->format(round($persona->salario_total, 0)) ?>).
    </p>

    <!-- SALARIO 70% / 30% - INACTIVO -->
    <?php elseif (!empty(trim($persona->bono_anual)) && !$persona->estado): ?>

    <p style="text-align: justify;">
        <b>GLORIA STELLA VELOZA FLAUTERO</b> identificada con cédula de ciudadanía No.52.886.080 de Bogotá, en su
        calidad de
        Subgerente de Recursos Humanos de la compañía <b>EZENTIS COLOMBIA S.A.S.</b>, sociedad legalmente constituida e
        identificada con Nit. 900.196.414-1 certifica que el <b>Sr.(a)
            <?php echo $persona->nombre_completo ?></b> identificado(a) con cédula de ciudadanía <b>No.
            <?php echo $persona->identificacion ?></b>, laboró en la compañía mediante contrato a término <b>
            <?php echo $persona->tipo_contrato ?></b> desde el día <b>
            <?php echo date_format($persona->fecha_ingreso, 'd/m/Y') ?></b>, hasta el dia <b>
            <?php echo date_format($persona->fecha_retiro, 'd/m/Y') ?>
        </b>, desempeñandose como
        <b>
            <?php echo $persona->cargo ?></b> con una asignación básica mensual de
        <?php echo \NumeroALetras::convertir(round($persona->salario_parcial, 0)) ?> PESOS
        M/CTE., ($
        <?php echo $this->Number->format(round($persona->salario_parcial, 0)) ?>) y una
        bonificación no constitutiva de salario anual de hasta
        <?php echo \NumeroALetras::convertir(round($persona->bono_anual, 0)) ?> PESOS M/CTE ($
        <?php echo $this->Number->format(round($persona->bono_anual, 0)) ?>).
    </p>

    <!-- SALARIO 70% / 30% - ACTIVO -->
    <?php elseif (!empty(trim($persona->bono_anual)) && $persona->estado): ?>

    <p style="text-align: justify;">
        <b>GLORIA STELLA VELOZA FLAUTERO</b> identificada con cédula de ciudadanía No.52.886.080 de Bogotá, en su
        calidad de
        Subgerente de Recursos Humanos de la compañía <b>EZENTIS COLOMBIA S.A.S.</b>, sociedad legalmente constituida e
        identificada con Nit. 900.196.414-1 certifica que el <b>Sr.(a)
            <?php echo $persona->nombre_completo ?></b> identificado(a) con cédula de ciudadanía <b>No.
            <?php echo $persona->identificacion ?></b>, labora en la compañía mediante contrato a término <b>
            <?php echo $persona->tipo_contrato ?></b> desde el día <b>
            <?php echo date_format($persona->fecha_ingreso, 'd/m/Y') ?></b>,
        desempeñandose como
        <b>
            <?php echo $persona->cargo ?></b> con una asignación básica mensual de
        <?php echo \NumeroALetras::convertir(round($persona->salario_parcial, 0)) ?> PESOS
        M/CTE., ($
        <?php echo $this->Number->format(round($persona->salario_parcial, 0)) ?>) y una
        bonificación no constitutiva de salario anual de hasta
        <?php echo \NumeroALetras::convertir(round($persona->bono_anual, 0)) ?> PESOS M/CTE ($
        <?php echo $this->Number->format(round($persona->bono_anual, 0)) ?>).
    </p>


    <?php else: ?>
        <p>OCURRIO UN ERROR</p>
        <p>POR FAVOR COMUNIQUESE CON EL DEPARTAMENTO DE RECURSOS HUMANOS</p>
        <?php exit() ?>

    <?php endif ?>

    <!-- PIE DE PAGINA / FIRMA -->
    <p style="text-align: justify;">
        Cualquier tipo de inquietud con gusto será atendido en la dirección Carrera 50 No.128b – 32 teléfono 4321200
        ext.
        1045-1046-1033 o al correo electrónico: gloria.veloza@ezentis.com.co, wilson.castaneda@ezentis.com.co,
        milena.tobon@col-ezentis.com
    </p>

    <p style="text-align: justify;">
        La presente certificación se expide a solicitud del interesado en la ciudad de Bogotá el día
        <?php echo $formatter->format($dt) ?>.
    </p>
    <p>
        Cordialmente,
    </p>
    <?php echo $this->Html->image('certificacion/footer.PNG', ['fullBase' => true, 'width' => '100%']) ?>

    <!-- //  -->
</body>

</html>

<?php //exit(); ?>
