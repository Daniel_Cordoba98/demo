<script>
    var htmlcontent =
        "<div class='progress-bar bg-green progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'><strong>SUBIENDO PLANTA</strong></div>";

    function showHtmlMessage() {
        swal({
            title: 'Cargando Archivo...',
            text: htmlcontent,
            html: true,
            showConfirmButton: false
        });
    }

</script>

<h3>Cargar Excel</h3>
<hr>

<div class="body">
    <?php echo $this->Form->create(null, ['type' => 'file']) ?>
    <?php echo $this->Form->file('archivo', ['class' => "form-control"]) ?>
    <hr>
    <div class="row clearfix js-sweetalert" style="margin-left: 3px;">
        <?php
            echo $this->Form->button('Enviar', [
                'class' => "btn btn-lg bg-green waves-effect",
                'data-type' => "html-message",
            ]);
            echo $this->Html->link('Descargar Formato', '/formatos/formato_planta.xlsx', [
                'class' => 'btn btn-lg bg-teal waves-effect',
                'style' => "margin-left: 3px;",
            ]);
        ?>
    </div>
    <?php echo $this->Form->end() ?>
</div>

<?php
    echo $this->Html->css(['/tema/plugins/sweetalert/sweetalert.css'], ['block' => 'css']);
    echo $this->Html->script(['/tema/plugins/sweetalert/sweetalert.min.js'], ['block' => 'script']);
    echo $this->Html->script(['/tema/js/pages/ui/dialogs.js'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/bootstrap-notify/bootstrap-notify.js'], ['block' => 'script']);
?>
