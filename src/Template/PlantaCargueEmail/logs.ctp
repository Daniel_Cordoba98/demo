<h3>Registros</h3>
<hr>
<?php foreach ($archivos as $key => $value): ?>

<li>
    <?php
        echo $this->Html->link($value, ['action' => 'logs_detalle', $value]);
    ?>
</li>
<hr style= "margin-top: 10px; margin-bottom: 10px;">
<?php endforeach ?>
<ul class="pagination">
    <li class="active">
        <?php for ($i = 0; $i <= $paginas - 1; $i++) {
                echo $this->Html->link($i + 1, ['action' => 'logs', $i], [
                    'class' => "waves-effect",
                    'style' => 'margin-left: 3px;',
                ]);
            }
        ?>
    </li>
</ul>
