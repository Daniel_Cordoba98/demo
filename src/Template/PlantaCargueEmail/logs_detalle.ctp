<h3>Detalle del registro</h3>
<hr>
<div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo_1">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseTwo_1"
                    aria-expanded="false" aria-controls="collapseTwo_1">
                    Archivos adjuntos
                </a>
            </h4>
        </div>

        <div id="collapseTwo_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo_1"
            aria-expanded="false">
            <div class="panel-body">
                <h5>
                    Mensaje:
                    <?php echo $detalle_log['archivos_adjuntos']['mensaje'] ?>
                </h5>

                <?php if (isset($detalle_log['archivos_adjuntos']['asuntos'])): ?>
                <div class="body table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Asunto de correo</th>
                            <th>Adjunto leído</th>
                        </tr>
                        <tbody>
                            <?php foreach ($detalle_log['archivos_adjuntos']['asuntos'] as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $value ?>
                                </td>
                                <td>
                                    <?php echo (isset($detalle_log['archivos_adjuntos']['archivos'][$key])) ? $detalle_log['archivos_adjuntos']['archivos'][$key] : '<i>Sin archivo o invalido</i>' ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>

    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo_2">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapseTwo_2"
                    aria-expanded="false" aria-controls="collapseTwo_2">
                    Datos acumulados
                </a>
            </h4>
        </div>
        <div id="collapseTwo_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_2"
            aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <?php if (isset($detalle_log['datos_acumulados'])): ?>
                <div class="body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>nombres</th>
                                <th>apellidos</th>
                                <th>apellidos2</th>
                                <th>usuario</th>
                                <th>identificacion</th>
                                <th>cargo</th>
                                <th>proyecto_str</th>
                                <th>proyecto_id</th>
                                <th>nueva_area</th>
                                <th>reemplazo</th>
                                <th>correo_personal</th>
                                <th>celular</th>
                                <th>tipo_sangre_id</th>
                                <th>fecha_nacimiento</th>
                                <th>fecha_ingreso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($detalle_log['datos_acumulados'] as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $value['nombres'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos2'] ?>
                                </td>
                                <td>
                                    <?php echo $value['usuario'] ?>
                                </td>
                                <td>
                                    <?php echo $value['identificacion'] ?>
                                </td>
                                <td>
                                    <?php echo $value['cargo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_str'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['nueva_area'] ?>
                                </td>
                                <td>
                                    <?php echo $value['reemplazo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['correo_personal'] ?>
                                </td>
                                <td>
                                    <?php echo $value['celular'] ?>
                                </td>
                                <td>
                                    <?php echo $value['tipo_sangre_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_nacimiento'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_ingreso'] ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>

    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo_3">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_3" href="#collapseTwo_3"
                    aria-expanded="false" aria-controls="collapseTwo_3">
                    Datos acumulados correo
                </a>
            </h4>
        </div>
        <div id="collapseTwo_3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_3"
            aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <?php if (isset($detalle_log['datos_acumulados_correo'])): ?>
                <div class="body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>nombres</th>
                                <th>apellidos</th>
                                <th>apellidos2</th>
                                <th>usuario</th>
                                <th>identificacion</th>
                                <th>cargo</th>
                                <th>proyecto_str</th>
                                <th>proyecto_id</th>
                                <th>nueva_area</th>
                                <th>reemplazo</th>
                                <th>correo_personal</th>
                                <th>celular</th>
                                <th>tipo_sangre_id</th>
                                <th>fecha_nacimiento</th>
                                <th>fecha_ingreso</th>
                                <th>creado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($detalle_log['datos_acumulados_correo'] as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $value['nombres'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos2'] ?>
                                </td>
                                <td>
                                    <?php echo $value['usuario'] ?>
                                </td>
                                <td>
                                    <?php echo $value['identificacion'] ?>
                                </td>
                                <td>
                                    <?php echo $value['cargo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_str'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['nueva_area'] ?>
                                </td>
                                <td>
                                    <?php echo $value['reemplazo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['correo_personal'] ?>
                                </td>
                                <td>
                                    <?php echo $value['celular'] ?>
                                </td>
                                <td>
                                    <?php echo $value['tipo_sangre_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_nacimiento'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_ingreso'] ?>
                                </td>
                                <td>
                                    <?php echo $value['creado'] ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>

    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo_4">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_4" href="#collapseTwo_4"
                    aria-expanded="false" aria-controls="collapseTwo_4">
                    Notificación TI
                </a>
            </h4>
        </div>
        <div id="collapseTwo_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_4"
            aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <?php if (isset($detalle_log['notificacion_ti'])): ?>
                <div class="body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th scope="row">result_envio</th>
                            <th>
                                <?php echo $detalle_log['notificacion_ti']['result_envio'] ?>
                            </th>
                            <tr>
                                <th>nombres</th>
                                <th>apellidos</th>
                                <th>apellidos2</th>
                                <th>usuario</th>
                                <th>identificacion</th>
                                <th>cargo</th>
                                <th>proyecto_str</th>
                                <th>proyecto_id</th>
                                <th>nueva_area</th>
                                <th>reemplazo</th>
                                <th>correo_personal</th>
                                <th>celular</th>
                                <th>tipo_sangre_id</th>
                                <th>fecha_nacimiento</th>
                                <th>fecha_ingreso</th>
                                <th>creado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($detalle_log['notificacion_ti'] as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $value['nombres'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos2'] ?>
                                </td>
                                <td>
                                    <?php echo $value['usuario'] ?>
                                </td>
                                <td>
                                    <?php echo $value['identificacion'] ?>
                                </td>
                                <td>
                                    <?php echo $value['cargo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_str'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['nueva_area'] ?>
                                </td>
                                <td>
                                    <?php echo $value['reemplazo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['correo_personal'] ?>
                                </td>
                                <td>
                                    <?php echo $value['celular'] ?>
                                </td>
                                <td>
                                    <?php echo $value['tipo_sangre_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_nacimiento'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_ingreso'] ?>
                                </td>
                                <td>
                                    <?php echo $value['creado'] ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>

    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo_5">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_5" href="#collapseTwo_5"
                    aria-expanded="false" aria-controls="collapseTwo_5">
                    Notificación lideres
                </a>
            </h4>
        </div>
        <div id="collapseTwo_5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_5"
            aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <?php if (isset($detalle_log['notificacion_lideres']['BUCLE'][0])): ?>
                <div class="body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    BUCLE
                                </th>
                            </tr>
                            <tr>
                                <th>nombres</th>
                                <th>apellidos</th>
                                <th>apellidos2</th>
                                <th>usuario</th>
                                <th>identificacion</th>
                                <th>cargo</th>
                                <th>proyecto_str</th>
                                <th>proyecto_id</th>
                                <th>nueva_area</th>
                                <th>reemplazo</th>
                                <th>correo_personal</th>
                                <th>celular</th>
                                <th>tipo_sangre_id</th>
                                <th>fecha_nacimiento</th>
                                <th>fecha_ingreso</th>
                                <th>creado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($detalle_log['notificacion_lideres']['BUCLE'][0] as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $value['nombres'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos'] ?>
                                </td>
                                <td>
                                    <?php echo $value['apellidos2'] ?>
                                </td>
                                <td>
                                    <?php echo $value['usuario'] ?>
                                </td>
                                <td>
                                    <?php echo $value['identificacion'] ?>
                                </td>
                                <td>
                                    <?php echo $value['cargo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_str'] ?>
                                </td>
                                <td>
                                    <?php echo $value['proyecto_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['nueva_area'] ?>
                                </td>
                                <td>
                                    <?php echo $value['reemplazo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['correo_personal'] ?>
                                </td>
                                <td>
                                    <?php echo $value['celular'] ?>
                                </td>
                                <td>
                                    <?php echo $value['tipo_sangre_id'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_nacimiento'] ?>
                                </td>
                                <td>
                                    <?php echo $value['fecha_ingreso'] ?>
                                </td>
                                <td>
                                    <?php echo $value['creado'] ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php endif ?>
            </div>

            <?php if (isset($detalle_log['notificacion_lideres']['BUCLE - ANTIOQUIA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                BUCLE - ANTIOQUIA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['BUCLE - ANTIOQUIA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['BUCLE - BOGOTA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                BUCLE - BOGOTA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['BUCLE - BOGOTA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['BUCLE - CHOCO'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                BUCLE - CHOCO
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['BUCLE - CHOCO'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['BUCLE - CUNDINAMARCA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                BUCLE - CUNDINAMARCA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['BUCLE - CUNDINAMARCA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['CCST'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                CCST
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['CCST'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['CGP'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                CGP
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['CGP'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['CONTROL PERDIDAS'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                CONTROL PERDIDAS
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['CONTROL PERDIDAS'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['CUADRILLAS CODENSA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                CUADRILLAS CODENSA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['CUADRILLAS CODENSA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['ESINCO'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                ESINCO
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['ESINCO'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['GERENCIA DESARROLLO DE NEGOCIOS'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                GERENCIA DESARROLLO DE NEGOCIOS
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['GERENCIA DESARROLLO DE NEGOCIOS'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['GERENCIA ENERGIA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                GERENCIA ENERGIA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['GERENCIA ENERGIA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['GERENCIA FINANCIERA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                GERENCIA FINANCIERA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['GERENCIA FINANCIERA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['GERENCIA GENERAL'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                GERENCIA GENERAL
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['GERENCIA GENERAL'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['GERENCIA RECURSOS'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                GERENCIA RECURSOS
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['GERENCIA RECURSOS'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['GERENCIA TECNOLOGIA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                GERENCIA TECNOLOGIA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['GERENCIA TECNOLOGIA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['GERENCIA TELECOMUNICACIONES'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                GERENCIA TELECOMUNICACIONES
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['GERENCIA TELECOMUNICACIONES'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['NOKIA'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                NOKIA
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['NOKIA'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>

            <?php if (isset($detalle_log['notificacion_lideres']['PLAN CALIDAD'][0])): ?>
            <div class="body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                PLAN CALIDAD
                            </th>
                        </tr>
                        <tr>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>apellidos2</th>
                            <th>usuario</th>
                            <th>identificacion</th>
                            <th>cargo</th>
                            <th>proyecto_str</th>
                            <th>proyecto_id</th>
                            <th>nueva_area</th>
                            <th>reemplazo</th>
                            <th>correo_personal</th>
                            <th>celular</th>
                            <th>tipo_sangre_id</th>
                            <th>fecha_nacimiento</th>
                            <th>fecha_ingreso</th>
                            <th>creado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle_log['notificacion_lideres']['PLAN CALIDAD'][0] as $key => $value): ?>
                        <tr>
                            <td>
                                <?php echo $value['nombres'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos'] ?>
                            </td>
                            <td>
                                <?php echo $value['apellidos2'] ?>
                            </td>
                            <td>
                                <?php echo $value['usuario'] ?>
                            </td>
                            <td>
                                <?php echo $value['identificacion'] ?>
                            </td>
                            <td>
                                <?php echo $value['cargo'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_str'] ?>
                            </td>
                            <td>
                                <?php echo $value['proyecto_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['nueva_area'] ?>
                            </td>
                            <td>
                                <?php echo $value['reemplazo'] ?>
                            </td>
                            <td>
                                <?php echo $value['correo_personal'] ?>
                            </td>
                            <td>
                                <?php echo $value['celular'] ?>
                            </td>
                            <td>
                                <?php echo $value['tipo_sangre_id'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_nacimiento'] ?>
                            </td>
                            <td>
                                <?php echo $value['fecha_ingreso'] ?>
                            </td>
                            <td>
                                <?php echo $value['creado'] ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>
        </div>
    </div>

    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo_6">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_6" href="#collapseTwo_6"
                    aria-expanded="false" aria-controls="collapseTwo_6">
                    Correos eliminados
                </a>
            </h4>
        </div>
        <div id="collapseTwo_6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_6"
            aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <?php if (isset($detalle_log['correos_eliminados'])): ?>
                <div class="body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>cedula</th>
                                <th>correo</th>
                                <th>resultado</th>
                                <th>resultado_planta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($detalle_log['correos_eliminados'] as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $value['cedula'] ?>
                                </td>
                                <td>
                                    <?php echo $value['correo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['resultado'] ?>
                                </td>
                                <td>
                                    <?php echo $value['resultado_planta'] ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo_7">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_7" href="#collapseTwo_7"
                    aria-expanded="false" aria-controls="collapseTwo_7">
                    Notificación TI eliminar
                </a>
            </h4>
        </div>
        <div id="collapseTwo_7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_7"
            aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <?php if (isset($detalle_log['correos_eliminados'])): ?>
                <div class="body table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th scope="row">result_envio</th>
                            <th>
                                <?php echo $detalle_log['notificacion_ti_eliminar']['result_envio'] ?>
                            </th>
                            <tr>
                                <th>cedula</th>
                                <th>correo</th>
                                <th>resultado</th>
                                <th>resultado_planta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($detalle_log['notificacion_ti_eliminar'] as $key => $value): ?>
                            <tr>
                                <td>
                                    <?php echo $value['cedula'] ?>
                                </td>
                                <td>
                                    <?php echo $value['correo'] ?>
                                </td>
                                <td>
                                    <?php echo $value['resultado'] ?>
                                </td>
                                <td>
                                    <?php echo $value['resultado_planta'] ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<hr>
<?php
    echo $this->Html->link(
        'Volver',
        $pagina__anterior,
        ['class' => "btn btn-lg btn-info waves-effect"]
    );
?>
