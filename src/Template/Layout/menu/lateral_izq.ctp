<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <strong>
                        <?php echo strtoupper($usuario_logueado['nombres']) ?>
                        <br>
                        <?php echo strtoupper($usuario_logueado['apellidos']) ?>
                    </strong>
                </div>
                <div class="email">
                    <?php echo $usuario_logueado['email'] ?>
                </div>
                <div class="email">
                    <?php echo h($usuario_logueado['rol']['descripcion']) ?>
                </div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li>

                            <?php echo $this->Html->link(
                                    '<i class="material-icons">person</i>Perfil',
                                    ['controller' => 'Users', 'action' => 'view', $usuario_logueado['id']],
                                    ['escape' => false]
                                );
                            ?>

                        </li>
                        <li role="separator" class="divider"></li>
                        <li>

                            <?php echo $this->Html->link(
                                    '<i class="material-icons">exit_to_app</i>Salir',
                                    ['controller' => 'Users', 'action' => 'logout'],
                                    ['escape' => false]
                                );
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <!-- <li class="header">EZENTIS</li> -->
                <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'Inicio') ? 'active' : '' ?>">
                    <?php echo $this->Html->link(
                            '<i class="material-icons">home</i><span>Inicio</span>',
                            ['controller' => 'Inicio', 'action' => 'index'],
                            ['escape' => false]
                    ); ?>

                </li>
                <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'Users') ? 'active' : '' ?>">
                    <?php echo $this->Html->link(
                            '<i class="material-icons">account_box</i><span>Usuarios</span>',
                            ['controller' => 'Users', 'action' => 'index'],
                            ['escape' => false]
                    ); ?>

                </li>
                <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'Empleados') ? 'active' : '' ?>
                    <?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'PlantaCargue') ? 'active' : '' ?>">
                    <a class="menu-toggle">
                        <i class="material-icons">people</i>
                        <span>Planta</span>
                    </a>
                    <ul class="ml-menu">

                        <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'Empleados') ? 'active' : '' ?>">
                            <?php echo $this->Html->link(
                                    'Empleados',
                                    ['controller' => 'Empleados', 'action' => 'index'],
                                    ['escape' => false]
                            ); ?>

                        </li>
                        <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'PlantaCargue') ? 'active' : '' ?>">
                            <?php echo $this->Html->link(
                                    'Carga Archivo',
                                    ['controller' => 'PlantaCargue', 'action' => 'index'],
                                    ['escape' => false]
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'AdministrarCorreo') ? 'active' : '' ?>">
                    <a class="menu-toggle">
                        <i class="material-icons">email</i>
                        <span>Correo corporativo</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('action') == 'restablecerContrasena') ? 'active' : '' ?>">
                            <?php echo $this->Html->link(
                                    'Cambiar contraseña',
                                    ['controller' => 'AdministrarCorreo', 'action' => 'restablecerContrasena'],
                                    ['escape' => false]
                            ); ?>
                        </li>
                        <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('action') == 'ldapIndex') ? 'active' : '' ?>">
                            <?php echo $this->Html->link(
                                    'Consulta Ldap',
                                    ['controller' => 'AdministrarCorreo', 'action' => 'ldapIndex'],
                                    ['escape' => false]
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'CumpleanosEmails') ? 'active' : '' ?>">
                    <?php echo $this->Html->link(
                            '<i class="material-icons">cake</i><span>Notificación Cumpleaños</span>',
                            ['controller' => 'CumpleanosEmails', 'action' => 'index'],
                            ['escape' => false]
                    ); ?>
                </li>
                <li class="<?php echo (!empty($this->request->getParam('controller')) && $this->request->getParam('controller') == 'PlantaCargueEmail') ? 'active' : '' ?>">
                    <?php echo $this->Html->link(
                            '<i class="material-icons">content_paste</i><span>Logs</span>',
                            ['controller' => 'PlantaCargueEmail', 'action' => 'logs'],
                            ['escape' => false]
                    ); ?>
                </li>
            </ul>
        </div>
        <!-- #Menu -->

        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2016 - 2017
                <a href="javascript:void(0);">AdminBSB - Material Design</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.5
            </div>
        </div>
        <!-- #Footer -->

    </aside>
    <!-- #END# Left Sidebar -->

</section>
