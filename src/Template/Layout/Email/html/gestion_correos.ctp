<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>

<head>
    <title>
        <?php echo $this->fetch('title') ?>
    </title>
</head>

<body>
    <?php echo $this->fetch('content') ?>
</body>
<br>
<hr>
<footer>

    <img src="logo.jpg" alt="">
    <p>
        <u>
            <span style="font-size: 9.0pt; font-family: 'Arial',sans-serif; color: #bd186d;">
                <a href="http://www.ezentis.com/">
                    <span style="color: #bd186d;">www.ezentis.com</span>
                </a>.co</span>
        </u>
    </p>
    <p>
        <span style="font-size: 9.0pt; font-family: 'Arial',sans-serif; color: #535758;">..................................................................................................................................</span>
    </p>
    <p>
        <strong>
            <span style="font-size: 9.0pt; font-family: 'Arial',sans-serif; color: #bd186d;">Ezentis</span>
        </strong>
    </p>
    <p>
        <span style="font-size: 9.0pt; font-family: 'Arial',sans-serif; color: #535758;">Carrera 50 No 128B &ndash; 32 Prado Veraniego</span>
    </p>
    <p>
        <span style="font-size: 9.0pt; font-family: 'Arial',sans-serif; color: #535758;">Bogot&aacute;&nbsp; Colombia</span>
    </p>
    <p>
        <span style="font-size: 9.0pt; font-family: 'Arial',sans-serif; color: #535758;">Tel 4321200&nbsp; ext 1032</span>
    </p>
    <p>
        <span style="font-size: 9.0pt; font-family: 'Arial',sans-serif; color: #535758;">..................................................................................................................................</span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">AVISO LEGAL.</span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">Este mensaje y sus documentos anexos pueden contener informaci&oacute;n confidencial o legalmente protegida. Est&aacute;
            dirigido &uacute;nica y exclusivamente a la persona o entidad rese&ntilde;ada como destinatarios del mensaje.
            Si este mensaje le hubiera llegado por error, por favor elim&iacute;nelo sin revisarlo ni reenviarlo y notif&iacute;quelo
            lo antes posible al remitente. Cualquier divulgaci&oacute;n, copia o utilizaci&oacute;n de dicha informaci&oacute;n
            es contraria a la ley. </span>
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">Le agradecemos su colaboraci&oacute;n. </span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">PRIVILEGED AND CONFIDENTIAL.</span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">This message and any attached documents may contain confidential information or information subject to professional
            secret. If you are not the intended recipient, please advise the sender immediately by replying to this e-mail
            and delete this message and any attachments without keeping any copy. Any disclosure, copying or unauthorized
            use of such information is contrary to law. </span>
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">Thank you for your help. </span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">MEDIOAMBIENTE.</span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">No imprima si no es necesario. Protejamos el medio ambiente.</span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">ENVIRONMENT.</span>
    </p>
    <p style="text-align: justify;">
        <span style="font-size: 7.5pt; font-family: 'Arial',sans-serif; color: #666666;">Print only if neccesary. Please take care of the environment.</span>
    </p>
</footer>

</html>
