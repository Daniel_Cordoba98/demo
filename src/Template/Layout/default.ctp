<!DOCTYPE html>
<html>

<head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php echo $this->fetch('meta') ?>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <?php
        echo $this->Html->charset('utf-8');

        echo $this->Html->css([
            '/tema/plugins/bootstrap/css/bootstrap',
            '/tema/plugins/node-waves/waves',
            '/tema/plugins/animate-css/animate',
            '/tema/css/style',
            '/tema/css/themes/all-themes',

            '/tema/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap',
            '/tema/plugins/jquery-datatable/skin/bootstrap/css/dataTables.searchHighlight',
        ]);
    ?>
    
    <?php echo $this->fetch('css') ?>

    <?php
        echo $this->Html->script([
            '/tema/plugins/jquery/jquery.min',
            '/tema/plugins/bootstrap/js/bootstrap',
            '/tema/plugins/bootstrap-select/js/bootstrap-select',
            '/tema/plugins/jquery-slimscroll/jquery.slimscroll',
            '/tema/plugins/node-waves/waves',
            '/tema/js/admin',
            '/tema/js/demo',

            '/tema/js/pages/ui/tooltips-popovers',
            '/tema/plugins/jquery-datatable/jquery.dataTables',
            '/tema/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap',
            '/tema/plugins/jquery-datatable/skin/bootstrap/js/dataTables.searchHighlight',
            '/tema/plugins/jquery-datatable/skin/bootstrap/js/jquery.highlight',

        ]);
    ?>

    <?php echo $this->fetch('script') ?>

</head>

<body class="theme-red">

    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->

    <?php include 'menu/superior.ctp'?>

    <?php include 'menu/lateral_izq.ctp'?>


    <section class="content">
        <div class="container-fluid">


            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">

                        <div class="body">

                            <?php echo $this->Flash->render() ?>
                            <?php echo $this->fetch('content') ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</body>

</html>
