<?php
    $this->Form->setTemplates([
        // Container element used by control().
        'inputContainer' => '{{content}}',

        // Container element used by control() when a field has an error.
        'inputContainerError' => '{{content}}',
    ]);
?>
    <h4>Restablecer contraseña correo corporativo</h4>
    <hr>

    <?php echo $this->Form->create() ?>

    <div class="row clearfix">
        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('correo_corporativo', [
                            'required' => true,
                            'label' => false,
                            'type' => 'email',
                            'class' => 'form-control',
                    ]) ?>
                    <label class="form-label">Correo corporativo</label>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('contrasena', [
                            'required' => true,
                            'label' => false,
                            'type' => 'password',
                            'class' => 'form-control',
                    ]) ?>
                    <label class="form-label">Nueva contraseña</label>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <?php echo $this->Form->input('requerir_cambio', [
                    'label' => false,
                    'id' => 'md_checkbox_30',
                    'type' => 'checkbox',
                    'class' => 'filled-in chk-col-green',
            ]) ?>
            <label for="md_checkbox_30">Requerir cambio</label>
        </div>
    </div>
    <hr>
    <div class="row" style="margin-left: 8px;">
        <?php
            echo $this->Form->button('Cambiar', [
                'class' => "btn btn-lg bg-green waves-effect",
            ]);

            echo $this->Html->link('Volver',
                $pagina__anterior, [
                    'class' => "btn btn-lg btn-info waves-effect",
                    'style' => "margin-left: 8px;",
                ]);
        ?>
    </div>

    <?php echo $this->Form->end() ?>
