<style>
.btn-circle {
    width: 32px;
    height: 32px;
}

.btn-circle i {
    left: -5px;
    top: -4px;
}
</style>

<div class="table-responsive">
    <table id="data_table" class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Acciones</th>
            </tr>
        </thead>
    </table>
</div>

<script>
$(document).ready(function () {

    var table = $('#data_table').DataTable({
        "processing": true,
        // "serverSide": true,
        "ajax": "<?php echo $this->Url->build(['controller' => 'AdministrarCorreo', 'action' => 'getCorreosLdap'], true) ?>",

        "language": {
            "lengthMenu": "Mostrar _MENU_ resultados",
            "zeroRecords": "No hay resultados...",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
            "infoFiltered": "(Filtrado de _MAX_ registros)",
            "search": "Buscar:",
            "processing": "Cargando...",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    table.on('draw.dt', function () {
        var body = $(table.table().body());
        body.unhighlight();
        body.highlight(table.search());
    });

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});
</script>
