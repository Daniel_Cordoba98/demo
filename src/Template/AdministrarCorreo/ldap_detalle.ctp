<style>
    li {
        margin-bottom: 8px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <h3>Informacion en servidor de la cuenta <u><?php echo $datos['zimbraMailDeliveryAddress'] ?></u></h3>
        <hr>

        <ul>
            <li><b>Correo: </b>
                <?php echo $datos['zimbraMailDeliveryAddress'] ?>
            </li>
            <li><b>Nombre Mostrado: </b>
                <?php echo $datos['displayName'] ?>
            </li>
            <li><b>Cedula: </b>
                <?php echo $datos['zimbraNotes'] ?>
            </li>
            <li><b>Descripción: </b>
                <?php echo $datos['description'] ?>
            </li>
            <li><b>Estado: </b>
                <?php echo $datos['zimbraAccountStatus'] ?>
            </li>
            <li><b>Fecha creado: </b>
                <?php echo $datos['zimbraCreateTimestamp'] ?>
            </li>
            <li><b>Fecha ult. inicio: </b>
                <?php echo $datos['zimbraLastLogonTimestamp'] ?>
            </li>
        </ul>

    </div>
</div>

<div class="row">

    <?php
        echo $this->Html->link(
            'Volver',
            $pagina__anterior,
            [
                'class' => "btn btn-lg btn-info waves-effect",
                'style' => 'margin-left: 30px;',
            ]
        );
    ?>
</div>
