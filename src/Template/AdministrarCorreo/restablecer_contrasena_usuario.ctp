<html>

<head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <?php
        echo $this->Html->charset('utf-8');
        echo $this->Html->css([
            '/tema/plugins/bootstrap/css/bootstrap',
            '/tema/plugins/node-waves/waves',
            '/tema/plugins/animate-css/animate',
            '/tema/css/style',
            '/tema/css/themes/all-themes',
        ]);
    ?>

        <?php
            echo $this->Html->script([
                '/tema/plugins/jquery/jquery.min',
                '/tema/plugins/bootstrap/js/bootstrap',
                '/tema/plugins/bootstrap-select/js/bootstrap-select',
                '/tema/plugins/jquery-slimscroll/jquery.slimscroll',
                '/tema/plugins/node-waves/waves',
                '/tema/js/admin',
                '/tema/js/demo',
                '/tema/plugins/jquery-inputmask/jquery.inputmask.bundle',
            ]);
        ?>
</head>


<?php
    $this->Form->setTemplates([
        // Container element used by control().
        'inputContainer' => '{{content}}',

        // Container element used by control() when a field has an error.
        'inputContainerError' => '{{content}}',

    ]);
?>

    <body class="login-page ls-closed">

        <div class="users form">
            <?php echo $this->Form->create($datos) ?>
            <div class="login-box">
                <div class="logo">
                    <a>
                        <b>Planta</b>
                    </a>
                    <small>Ezentis Colombia -
                        <?php echo date('Y') ?>
                    </small>
                </div>
                <div class="card">
                    <div class="body">
                        <h4 style="text-align: center;">Restablecer contraseña</h4>
                        <hr>
                        <?php
                            echo $this->Flash->render();
                            echo $this->Flash->render('Error');
                        ?>
                        <div class="row clearfix">

                            <?php if ($validaciones_ok): ?>

                            <p style="text-align: center;">Restableciendo contraseña del correo
                                <strong>
                                    <?php echo $this->request->getData('correo_corporativo') ?>
                                </strong>
                            </p>
                            <hr>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <?php echo $this->Form->input('contrasena1', [
                                                'required' => true,
                                                'type' => 'password',
                                                'label' => false,
                                                'class' => 'form-control',
                                        ]) ?>
                                        <label class="form-label">Nueva Contraseña</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <?php echo $this->Form->input('contrasena2', [
                                                'required' => true,
                                                'type' => 'password',
                                                'label' => false,
                                                'class' => 'form-control',
                                        ]) ?>
                                        <label class="form-label">Repita la nueva Contraseña</label>
                                    </div>
                                </div>
                            </div>

                            <?php
                                echo $this->Form->hidden('correo_corporativo');
                                echo $this->Form->hidden('identificacion');
                                echo $this->Form->hidden('fecha_nacimiento');
                                echo $this->Form->hidden('correo_personal');
                            ?>

                            <?php else: ?>

                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <?php echo $this->Form->input('correo_corporativo', [
                                                    'required' => true,
                                                    'type' => 'email',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                            ]) ?>
                                            <label class="form-label">Correo corporativo</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <?php echo $this->Form->input('identificacion', [
                                                    'required' => true,
                                                    'type' => 'number',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                            ]) ?>
                                            <label class="form-label">Identificación</label>
                                        </div>
                                        <small style="margin-top: 10px;">Numero de identificacion del empleado, sin puntos ni comas.</small>
                                    </div>
                                </div>

                                <div class="demo-masked-input">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <div class="form-line">
                                                <?php echo $this->Form->input('fecha_nacimiento', [
                                                        'required' => true,
                                                        'type' => 'text',
                                                        'label' => false,
                                                        'class' => 'form-control date',
                                                        'placeholder' => 'dd/mm/aaaa',
                                                ]) ?>
                                            </div>
                                            <small style="margin-top: 10px;">Fecha de nacimiento (dia/mes/año)</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <?php echo $this->Form->input('correo_personal', [
                                                    'required' => true,
                                                    'type' => 'email',
                                                    'label' => false,
                                                    'class' => 'form-control',
                                            ]) ?>
                                            <label class="form-label">Correo personal</label>
                                        </div>
                                        <small style="margin-top: 10px;">Correo personal registrado al momento de la contratación</small>
                                    </div>
                                </div>
                            <?php endif?>
                        </div>
                        <hr>
                        <div class="row" style="text-align: center;">
                            <?php
                                if ($this->request->is(['post', 'put'])) {
                                    echo $this->Html->link('Cancelar', ['action' => 'restablecerContrasenaUsuario'], [
                                        'class' => "btn btn-lg bg-red waves-effect",
                                        'style' => 'margin-right: 8px;',
                                    ]);
                                }
                                echo $this->Form->button('Restablecer', [
                                    'class' => "btn btn-lg bg-cyan waves-effect",
                                ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end() ?>
        </div>

        <script>
            $(document).ready(function () {

                //Date
                $('.date').inputmask('dd/mm/yyyy', {
                    placeholder: 'dd/mm/aaaa'
                });
            });
        </script>
    </body>
</html>
