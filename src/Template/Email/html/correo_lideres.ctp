<p>
    <?php echo $cabecera ?>
</p>

<table border=1>
    <tr>
        <th>NOMBRE</th>
        <th>USUARIO</th>
        <th>IDENTIFICACION</th>
        <th>CARGO</th>
        <th>PROYECTO</th>
    </tr>

    <?php foreach ($datos as $key => $value): ?>
    <tr>
        <td>
            <?php echo $value['nombres'] ?>
            <?php echo $value['apellidos'] ?>
            <?php echo $value['apellidos2'] ?>
        </td>
        <td>
            <?php echo $value['usuario'] ?>
        </td>
        <td>
            <?php echo $value['identificacion'] ?>
        </td>
        <td>
            <?php echo $value['cargo'] ?>
        </td>
        <td>
            <?php echo $value['proyecto_str'] ?>
        </td>
    </tr>
    <?php endforeach ?>
</table>

<p>
    La contraseña inicial de las cuentas corresponde a <b>Ez. + Número de identificación de cada usuario</b>, sin puntos 
    ni comas (Ej.: Ez.1234567), la cual requerirá ser cambiada al primer inicio de sesión en el 
    <a href=" http://intranet.ezentis.com.co/web/correo-corporativo" target="_blank">webmail</a>.
    <br>

    <br> Acceso al webmail: <a href="http://intranet.ezentis.com.co/web/correo-corporativo" target="_blank">http://intranet.ezentis.com.co/web/correo-corporativo</a>
    <br>

    <br> Cualquier inquietud por favor informar al correo aplicativos@ezentis.com.co
    <br>

    <br> Saludos.
    <br>

    <br> Fecha de creación:
    <?php echo date(" Y-m-d H:i:s") ?>
</p>
