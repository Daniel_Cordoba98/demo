<?php
    $this->Form->setTemplates([
        // Container element used by control().
         'inputContainer' => '{{content}}',

        // Container element used by control() when a field has an error.
         'inputContainerError' => '{{content}}',

    ]);
?>

    <style>
        .bootstrap-select {
            width: 100% !important;
        }

        .btn {
            margin-right: 8px !important;
        }

        form * :not(a) {
            cursor: default !important;
        }

    </style>

    <h3>Crear Empleado</h3>

    <hr>

    <?php echo $this->Form->create($planta) ?>

    <div class="row clearfix">

        <div class="col-sm-2">
            <?php echo $this->Form->input('clasificacion_id',
                    ['type' => 'select',
                        'options' => $clasificacion,
                        'class' => 'show-tick',
                        'label' => 'Clasificación',
                        'disabled' => true,
                ]) ?>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <div class="form-line">
                    <?php echo $this->Form->input('centro_de_costo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Centro de Costo',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <?php echo $this->Form->input('proyecto_id',
                    ['type' => 'select',
                        'options' => $proyecto,
                        'class' => 'show-tick',
                        'label' => 'Proyecto',
                        'disabled' => true,
                ]) ?>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('nueva_area', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Area',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('cargo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Cargo',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('reemplazo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Reemplazo',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <?php echo $this->Form->input('tipo_identificacion', [
                    'type' => 'select',
                    'options' => [
                        'CC' => 'CC',
                        'NIT' => 'NIT',
                        'TI' => 'TI',
                        'PEP' => 'PEP',
                        'P' => 'P',
                        'CE' => 'CE',
                    ],
                    'label' => 'Tipo Documento',
                    'class' => 'show-tick',
                    'disabled' => true,
            ]) ?>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('identificacion', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Número Documento',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('nombres', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Nombres',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('apellido_paterno', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Apellido Paterno',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('apellido_materno', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Apellido Materno',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('genero', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Genero',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('ciudad_trabajo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Ciudad de Trabajo',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('direccion_residencia', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Dirección de Residencia',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('departamento', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Departamento',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('correo_personal', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Correo Personal',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('correo_corporativo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Correo Corporativo',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('contacto_personal_fijo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Contacto Personal Fijo',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('contacto_personal_celular', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Contacto Personal Celular',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('fecha_nacimiento', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Fecha de Nacimiento',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('ciudad_nacimiento', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Ciudad de Nacimiento',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('porcentaje_riesgo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Porcentaje de Riesgo',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('eps', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'EPS',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('fondo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Fondo de Empleados',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('cesantias', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Fondo de Cesantias',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('arl', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'ARL',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('tipo_contrato', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Tipo Contrato',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('fecha_ingreso', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Fecha de ingreso',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('fecha_retiro', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Fecha de Retiro',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('reingreso', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Reingreso',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('num_hijos', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Numero de Hijos',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <?php echo $this->Form->input('tipo_sangre_id',
                    ['type' => 'select',
                        'options' => $tipo_sangre,
                        'class' => 'show-tick',
                        'label' => 'Tipo de Sangre',
                        'disabled' => true,
                ]) ?>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('estado_civil', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Estado Civil',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('cuenta', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Cuenta Bancaria',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('carne_cliente', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Carné de Cliente',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('carne_ezentis', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Carné de Ezentis',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('resultados_examenes_ingreso', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Resultados Examenes de Ingreso',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('examenes_ingreso_enfasis', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Resultados Examenes (Énfasis)',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('caja_compensacion', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Caja de Compensación',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('herramientcliente_t_t', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Herramienta Cliente',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('titulo_academico', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Titulo Academico',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('contrato', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Contrato',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <?php echo $this->Form->input('categoria_id',
                    ['type' => 'select',
                        'options' => $categoria,
                        'class' => 'show-tick',
                        'label' => 'Categoría',
                        'empty' => 'SELECCIONAR',
                        'disabled' => true,
                ]) ?>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('motivo_retiro', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Motivo de Retiro',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('mes_ano_reporte', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Mes y Año de Reporte',
                            'disabled' => true,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-10">
            <?php echo $this->Form->input('estado', [
                    'id' => 'basic_checkbox_3',
                    'type' => 'checkbox',
                    'label' => false,
                    'disabled' => true,
            ]) ?>
            <label for="basic_checkbox_3">Activo</label>

        </div>
    </div>

    <hr>

    <div class="row" style="margin-left: 8px;">
        <?php
            echo $this->Html->link(
                'Volver',
                $pagina__anterior,
                ['class' => "btn btn-lg btn-info waves-effect"]
            );
        ?>
    </div>

    <?php echo $this->Form->end() ?>

    <hr>

    <?php

        echo $this->Html->css(['/tema/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker'], ['block' => 'css']);
        echo $this->Html->css(['/tema/plugins/bootstrap-select/css/bootstrap-select'], ['block' => 'css']);
        echo $this->Html->css(['/tema/plugins/sweetalert/sweetalert'], ['block' => 'css']);
        echo $this->Html->script(['/tema/plugins/sweetalert/sweetalert.min'], ['block' => 'script']);
        echo $this->Html->script(['/tema/plugins/jquery-validation/jquery.validate'], ['block' => 'script']);
        echo $this->Html->script(['/tema/plugins/jquery-steps/jquery.steps'], ['block' => 'script']);
        echo $this->Html->script(['/tema/js/pages/forms/form-validation'], ['block' => 'script']);
        echo $this->Html->script(['/tema/plugins/momentjs/moment'], ['block' => 'script']);
        echo $this->Html->script(['/tema/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker'], ['block' => 'script']);
        echo $this->Html->script(['/tema/plugins/autosize/autosize'], ['block' => 'script']);
        echo $this->Html->script(['/tema/js/pages/forms/basic-form-elements'], ['block' => 'script']);

    ?>
