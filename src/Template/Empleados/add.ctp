<?php
    $this->Form->setTemplates([
        // Container element used by control().
         'inputContainer' => '{{content}}',

        // Container element used by control() when a field has an error.
         'inputContainerError' => '{{content}}',

    ]);
?>

<style>
    .bootstrap-select {
        width: 100% !important;
    }

    .btn {
        margin-right: 8px !important;
    }

    .error-message {
        color: red;
        font-size: 12px;
    }

    .dtp .dtp-buttons .dtp-btn-ok {
        background: #8BC34A;
    }

    .btn.btn-default.dtp-select-year-range {
        box-shadow: none !important;
    }

    .dtp-select-year-range.btn:not(.btn-raised):not(.btn-link):hover {
        background-color: rgba(153, 153, 153, .2) !important;
    }

</style>

<h3>Crear Empleado</h3>

<hr>

<?php echo $this->Form->create($planta) ?>

<div class="row clearfix">

    <div class="col-sm-2">
        <?php echo $this->Form->input('clasificacion_id',
                ['type' => 'select',
                    'options' => $clasificacion,
                    'class' => 'show-tick',
                    'label' => false,
                    'empty' => 'CLASIFICACION',
            ]) ?>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Centro de Costo</label>
                <?php echo $this->Form->input('centro_de_costo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <?php echo $this->Form->input('proyecto_id',
                ['type' => 'select',
                    'options' => $proyecto,
                    'class' => 'show-tick',
                    'label' => false,
                    'empty' => 'PROYECTO',
            ]) ?>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Area</label>
                <?php echo $this->Form->input('nueva_area', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Cargo</label>
                <?php echo $this->Form->input('cargo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Reemplazo</label>
                <?php echo $this->Form->input('reemplazo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <?php echo $this->Form->input('tipo_identificacion',
                ['type' => 'select',
                    'options' => [
                        'CC' => 'CC',
                        'NIT' => 'NIT',
                        'TI' => 'TI',
                        'PEP' => 'PEP',
                        'P' => 'P',
                        'CE' => 'CE',
                    ],
                    'label' => false,
                    'class' => 'show-tick',
                    'empty' => 'Tipo Documento',
            ]) ?>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Número Documento</label>
                <?php echo $this->Form->input('identificacion', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Nombres</label>
                <?php echo $this->Form->input('nombres', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Apellido Paterno</label>
                <?php echo $this->Form->input('apellido_Paterno', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Apellido Materno</label>
                <?php echo $this->Form->input('apellido_materno', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Genero</label>
                <?php echo $this->Form->input('genero', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Ciudad de Trabajo</label>
                <?php echo $this->Form->input('ciudad_trabajo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Direccion de Residencia</label>
                <?php echo $this->Form->input('direccion_residencia', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Departamento</label>
                <?php echo $this->Form->input('departamento', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3" style="margin-bottom: 0px;">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Correo Personal</label>
                <?php echo $this->Form->input('correo_personal', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
            <?php
                if ($this->Form->isFieldError('correo_personal')) {
                    echo $this->Form->error('correo_personal');
                }
            ?>
        </div>
    </div>

    <div class="col-sm-3" style="margin-bottom: 0px;">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Correo Corporativo</label>
                <?php echo $this->Form->input('correo_corporativo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
            <?php
                if ($this->Form->isFieldError('correo_corporativo', [
                ])) {
                    echo $this->Form->error('correo_corporativo');
                }
            ?>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Contacto Personal Fijo</label>
                <?php echo $this->Form->input('contacto_personal_fijo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Contacto Personal Celular</label>
                <?php echo $this->Form->input('contacto_personal_celular', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Fecha de Nacimiento</label>
                <?php echo $this->Form->input('fecha_nacimiento', [
                        'type' => "text",
                        'class' => "datepicker form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Ciudad de Nacimiento</label>
                <?php echo $this->Form->input('ciudad_nacimiento', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Porcentaje de Riesgo</label>
                <?php echo $this->Form->input('porcentaje_riesgo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">EPS</label>
                <?php echo $this->Form->input('eps', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Fondo de Empleados</label>
                <?php echo $this->Form->input('fondo', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Fondo de Cesantias</label>
                <?php echo $this->Form->input('cesantias', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">ARL</label>
                <?php echo $this->Form->input('arl', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>
    
    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Tipo Contrato</label>
                <?php echo $this->Form->input('tipo_contrato', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Fecha de Ingreso</label>
                <?php echo $this->Form->input('fecha_ingreso', [
                        'type' => "text",
                        'class' => "datepicker form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Fecha de Retiro</label>
                <?php echo $this->Form->input('fecha_retiro', [
                        'type' => "text",
                        'class' => "datepicker form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Reingreso</label>
                <?php echo $this->Form->input('reingreso', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Numero de Hijos</label>
                <?php echo $this->Form->input('num_hijos', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <?php echo $this->Form->input('tipo_sangre_id',
                ['type' => 'select',
                    'options' => $tipo_sangre,
                    'class' => 'show-tick',
                    'label' => false,
                    'empty' => 'TIPO DE SANGRE',
            ]) ?>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Estado Civil</label>
                <?php echo $this->Form->input('estado_civil', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Cuenta Bancaria</label>
                <?php echo $this->Form->input('cuenta', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Carné de Cliente</label>
                <?php echo $this->Form->input('carne_cliente', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Carné de Ezentis</label>
                <?php echo $this->Form->input('carne_ezentis', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Resultados Examenes de Ingreso</label>
                <?php echo $this->Form->input('resultados_examenes_ingreso', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Resultados Examenes (Énfasis)</label>
                <?php echo $this->Form->input('examenes_ingreso_enfasis', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Caja de Compensación</label>
                <?php echo $this->Form->input('caja_compensacion', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Herramienta Cliente</label>
                <?php echo $this->Form->input('herramientcliente_t_t', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Titulo Academico</label>
                <?php echo $this->Form->input('titulo_academico', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Contrato</label>
                <?php echo $this->Form->input('contrato', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <?php echo $this->Form->input('categoria_id',
                ['type' => 'select',
                    'options' => $categoria,
                    'class' => 'show-tick',
                    'label' => false,
                    'empty' => 'CATEGORÍA',
            ]) ?>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Motivo de Retiro</label>
                <?php echo $this->Form->input('motivo_retiro', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group form-float">
            <div class="form-line">
                <label class="form-label">Mes y Año de Reporte</label>
                <?php echo $this->Form->input('mes_ano_reporte', [
                        'type' => "text",
                        'class' => "form-control",
                        'label' => false,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="col-sm-10">
        <?php echo $this->Form->input('estado', [
                'id' => 'md_checkbox_10',
                'type' => 'checkbox',
                'class' => 'chk-col-light-green',
                'label' => false,
        ]) ?>
        <label for="md_checkbox_10">Activo</label>

    </div>
</div>

<hr>

<div class="row" style="margin-left: 8px;">
    <?php
        echo $this->Form->button(
            'Guardar',
            ['class' => "btn btn-lg bg-green waves-effect"]
        );
        echo $this->Html->link(
            'Volver',
            ['action' => 'index'],
            ['class' => "btn btn-lg btn-info waves-effect"]
        );
    ?>
</div>

<?php echo $this->Form->end() ?>

<hr>

<script>
    $(".datepicker").bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        time: false,
        lang: 'es',
        cancelText: 'Cancelar',
        okText: 'Aceptar',
        clearText: 'Borrar'
        switchOnClick: true
    });

</script>

<?php

    echo $this->Html->css(['/tema/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker'], ['block' => 'css']);
    echo $this->Html->css(['/tema/plugins/bootstrap-select/css/bootstrap-select'], ['block' => 'css']);
    echo $this->Html->css(['/tema/plugins/sweetalert/sweetalert'], ['block' => 'css']);
    echo $this->Html->script(['/tema/plugins/sweetalert/sweetalert.min'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/jquery-validation/jquery.validate'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/jquery-steps/jquery.steps'], ['block' => 'script']);
    echo $this->Html->script(['/tema/js/pages/forms/form-validation'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/momentjs/moment'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/momentjs/moment-with-locales'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/autosize/autosize'], ['block' => 'script']);
    echo $this->Html->script(['/tema/js/pages/forms/basic-form-elements'], ['block' => 'script']);

?>
