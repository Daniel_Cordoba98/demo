<?php
    $this->Form->setTemplates([
        // Container element used by control().
         'inputContainer' => '{{content}}',

        // Container element used by control() when a field has an error.
         'inputContainerError' => '{{content}}',

    ]);
?>

<style>
    .bootstrap-select {
        width: 100% !important;
    }

    .error-message {
        color: red;
        font-size: 12px;
    }

</style>


<h3>Gestión de Planta</h3>
<hr>
<?php echo $this->Html->link(
        'Crear Empleado',
        ['action' => 'add'],
        ['class' => 'btn bg-teal btn-lg waves-effect',
    ]) ?>
&nbsp;
<?php echo $this->Html->link(
        'Generar Reporte',
        ['action' => 'reportes'],
        ['class' => 'btn bg-teal btn-lg waves-effect',
    ]) ?>

<hr>

<h4>Filtro Planta</h4>

<?php echo $this->Form->create() ?>
<form>
    <div class="row clearfix">
        <div class="col-sm-4">
            <?php echo $this->Form->input('proyecto_id',
                    ['type' => 'select',
                        'options' => $proyecto,
                        'class' => 'show-tick',
                        'label' => false,
                        'empty' => 'PROYECTO',
                ]) ?>
        </div>
        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Nombres</label>
                    <?php echo $this->Form->input('nombre_completo', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => false,
                    ]) ?>
                </div>
            </div>
        </div>
</form>
<div class="col-sm-4">
    <div class="form-group form-float">
        <div class="form-line">
            <label class="form-label">Número Documento</label>
            <?php echo $this->Form->input('identificacion', [
                    'type' => "text",
                    'class' => "form-control",
                    'label' => false,
            ]) ?>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <?php echo $this->Form->button(
            '<i class="material-icons">search</i> BUSCAR', [
                'escape' => false,
                'type' => 'submit',
            'class' => 'btn bg-blue waves-effect']); ?>

    <?php echo $this->Html->link(
            '<i class="material-icons">backspace</i> LIMPIAR',
            ['action' => 'index'],
            ['class' => 'btn bg-blue btn lg waves-effect',
                'escape' => false,
        ]) ?>
</div>
<?php echo $this->Form->end() ?>
</div>

<hr>

<div>
    <table class="table responsive">
        <thead>
            <tr>
                <th scope="col">
                    <?php echo $this->Paginator->sort('identificacion') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('nombres', 'Nombre Completo') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('correo_corporativo') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('proyecto_id', 'Proyecto') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('estado') ?>
                </th>
                <th scope="col" style="min-width: 120px;">
                    <?php echo ('Acciones') ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($planta as $Empleados): ?>
            <tr>
                <td>
                    <?php echo h($Empleados->identificacion) ?>
                </td>
                <td>
                    <?php echo h($Empleados->nombre_completo) ?>
                </td>
                <td>
                    <?php echo h($Empleados->correo_corporativo) ?>
                </td>
                <td>
                    <?php echo h($Empleados->proyecto['descripcion']) ?>
                </td>
                <td>
                    <?php if ($Empleados->estado == true): ?>
                    <b>
                        <p style="color:#01DF01;">ACTIVO</p>
                    </b>
                    <?php else: ?>
                    <b>
                        <p style="color:red;">INACTIVO</p>
                    </b>
                    <?php endif ?>
                </td>
                <td class="btn-group btn-group-xs" style="padding: 0px;border: 0px; margin-top: 10px; display:inline-flex;">

                    <?php
                        echo $this->Form->create(null, ['url' => ['controller' => 'CertificacionLaboral', 'action' => ' index']]);
                        echo $this->Form->hidden('identificacion', [
                            'class' => "form-control",
                            'label' => false,
                            'name' => 'cedula',
                            'value' => $Empleados->identificacion,
                        ]);
                        echo $this->Form->button('<i style="margin-left:-6px; margin-top:-6px;" class="material-icons">description</i>', [
                            'escape' => false,
                            'type' => 'submit',
                            'data-toggle' => 'tooltip',
                            'data-original-title' => 'Certificado',
                            'class' => 'btn bg-green waves-effect',
                            'role' => "group",
                            'aria-label' => "Extra-small button group",
                            'style' => "height:27px; width:30px; border-radius:0px;"
                        ]);
                        echo $this->Form->end();
                    ?>

                    <?php echo $this->Html->link(
                            '<i class="material-icons">short_text</i>',
                            ['action' => 'view', $Empleados->id],
                            [
                                'escape' => false,
                                'style' => 'padding-top: 6px;',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => 'Ver',
                                'class' => 'btn bg-teal waves-effect',
                                'role' => "group",
                                'aria-label' => "Extra-small button group",
                                'style' => "padding-top: 1px;",
                            ])
                    ?>

                    <?php echo $this->Html->link(
                            '<i class="material-icons col-blue">edit</i>',
                            ['action' => 'edit', $Empleados->id],
                            [
                                'escape' => false,
                                'style' => 'padding-top: 6px;',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => 'Editar',
                                'class' => 'btn bg-blue waves-effect',
                                'role' => "group",
                                'aria-label' => "Extra-small button group",
                                'style' => "padding-top: 1px;",
                            ])
                    ?>

                    <?php echo $this->Form->postLink(
                            '<i class="material-icons col-blue">delete</i>',
                            ['action' => 'delete',
                                $Empleados->id],
                            [
                                'escape' => false,
                                'style' => 'padding-top: 6px;',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => 'Eliminar',
                                'class' => 'btn bg-light-blue waves-effect',
                                'role' => "group",
                                'aria-label' => "Extra-small button group",
                                'style' => "padding-top: 1px; border-radius: 0px;",
                                'confirm' => __('Esta seguro de eliminar el usuario {0}?', $Empleados->nombre_completo
                                )])
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?php echo $this->Paginator->first('<< ' . ('Primero')) ?>
            <?php echo $this->Paginator->prev('< ' . ('Anterior')) ?>
            <?php echo $this->Paginator->numbers() ?>
            <?php echo $this->Paginator->next(('Siguiente') . ' >') ?>
            <?php echo $this->Paginator->last(('Ultimo') . ' >>') ?>
        </ul>
        <p>
            <?php echo $this->Paginator->counter(['format' => ('Página {{page}} de {{pages}}, Mostrando {{current}} Registro(s) de {{count}}')]) ?>
        </p>
    </div>
</div>

<?php
    echo $this->Html->css(['/tema/plugins/animate-css/animate'], ['block' => 'css']);
    echo $this->Html->script(['/tema/js/pages/ui/tooltips-popovers'], ['block' => 'script']);
    echo $this->Html->script(['/tema/js/pages/ui/modals'], ['block' => 'script']);
    echo $this->Html->script(['/tema/js/pages/ui/notifications'], ['block' => 'script']);
    echo $this->Html->css(['/tema/plugins/bootstrap-select/css/bootstrap-select'], ['block' => 'css']);
    echo $this->Html->script(['/tema/js/pages/forms/basic-form-elements'], ['block' => 'script']);
    echo $this->Html->script(['/tema/js/pages/forms/form-validation'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/momentjs/moment'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/autosize/autosize'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/jquery-validation/jquery.validate'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/jquery-steps/jquery.steps'], ['block' => 'script']);
    echo $this->Html->css(['/tema/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker'], ['block' => 'css']);
    echo $this->Html->script(['/tema/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker'], ['block' => 'script']);

?>
