<?php
    $cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>

<head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <?php
        echo $this->Html->charset('utf-8');
        echo $this->Html->css([
            '/tema/plugins/bootstrap/css/bootstrap',
            '/tema/plugins/node-waves/waves',
            '/tema/plugins/animate-css/animate',
            '/tema/css/style',
            '/tema/css/themes/all-themes',
        ]);

    ?>

        <?php
            echo $this->Html->script([
                '/tema/plugins/jquery/jquery.min',
                '/tema/plugins/bootstrap/js/bootstrap',
                '/tema/plugins/bootstrap-select/js/bootstrap-select',
                '/tema/plugins/jquery-slimscroll/jquery.slimscroll',
                '/tema/plugins/node-waves/waves',
                '/tema/js/admin',
                '/tema/js/demo',
            ]);
        ?>
</head>


<body class="login-page ls-closed">
    <div class="users form">

        <?php echo $this->Form->create()?>
            <div class="login-box">
                <div class="logo">
                    <a href="javascript:void(0);">
                        <b>Planta</b>
                    </a>
                    <small>Ezentis Colombia - 
                    <?php echo date('Y') ?>
                    </small>
                </div>
                <div class="card">
                    <div class="body">
                        <form id="sign_in" method="POST" novalidate="novalidate">
                            <div class="msg">Iniciar Sesión</div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" aria-required="true">
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" placeholder="Password" required="" aria-required="true">
                                </div>
                            </div>

                            <div class="row">
                            
                            <?php echo $this->Flash->render() ?>

                            </div>

                            <div class="row">
                                <!-- <div class="col-xs-8 p-t-5">
                                    <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                                    <label for="rememberme">Recordarme</label>
                                </div> -->
                                <div class="col-xs-4">
                                    <button class="btn btn-block bg-cyan waves-effect" type="submit">INGRESAR</button>
                                </div>
                            </div>
                            <div class="row m-t-15 m-b--20">
                                <!-- <div class="col-xs-6">
                                    <a href="sign-up.html">Registrate!</a>
                                </div>
                                <div class="col-xs-6 align-right">
                                    <a href="forgot-password.html">Recuperar Contraseña</a>
                                </div> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end()?>
    </div>
</body>
</html>