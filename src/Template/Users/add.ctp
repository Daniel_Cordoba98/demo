<?php
    $this->Form->setTemplates([
        // Container element used by control().
        'inputContainer' => '{{content}}',

        // Container element used by control() when a field has an error.
        'inputContainerError' => '{{content}}',

    ]);
?>

    <style>
        .bootstrap-select {
            width: 100% !important;
        }

        .btn {
            margin-right: 8px !important;
        }

        .error-message {
            color: red;
            font-size: 12px;
        }

    </style>

    <h3>Crear Usuario</h3>

    <hr>


    <?php echo $this->Form->create($user) ?>


    <div class="row clearfix">

        <div class="col-sm-3">
            <?php echo $this->Form->input('tipo_documento',
                    ['type' => 'select',
                        'options' => [
                            'CC' => 'CC',
                            'NIT' => 'NIT',
                            'TI' => 'TI',
                        ],
                        'label' => false,
                        'class' => 'show-tick',
                        'empty' => 'Tipo Documento',
                ]) ?>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Número Documento</label>
                    <?php echo $this->Form->input('documento', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => false,
                    ]) ?>
                </div>

            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Nombres</label>
                    <?php echo $this->Form->input('nombres', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => false,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Apellidos</label>
                    <?php echo $this->Form->input('apellidos', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => false,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <!-- <span class="input-group-addon">
               <i class="material-icons">person</i>
            </span> -->
                <div class="form-line">
                    <label class="form-label">Nombre Usuario</label>
                    <?php echo $this->Form->input('username', [
                            'type' => "text",
                            'class' => "form-control date",
                            'label' => false,
                            // 'style' => 'position: unset'
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Contraseña</label>
                    <?php echo $this->Form->input('password', [
                            'type' => "password",
                            'class' => "form-control",
                            'label' => false,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-5">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">E-mail</label>
                    <?php echo $this->Form->input('email', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => false,
                    ]) ?>
                </div>

                <?php
                    if ($this->Form->isFieldError('email')) {
                        echo $this->Form->error('email');
                    }
                ?>
            </div>
        </div>

        <div class="col-sm-5">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Teléfono</label>
                    <?php echo $this->Form->input('telefono', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => false,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <?php echo $this->Form->input('rol_id',
                    ['type' => 'select',
                        'options' => $roles,
                        'class' => 'show-tick',
                        'label' => false,
                        'empty' => 'SELECCIONAR ROL',
                ]) ?>
        </div>

        <div class="col-sm-10">
            <?php echo $this->Form->input('activo', [
                    'id' => 'md_checkbox_10',
                    'type' => 'checkbox',
                    'class' => 'chk-col-light-green',
                    'label' => false,
                    'checked' => true,
            ]) ?>
            <label for="md_checkbox_10">Activo</label>

        </div>
    </div>

    <hr>

    <div class="row" style="margin-left: 8px;">
        <?php
            echo $this->Form->button(
                'Guardar',
                ['class' => "btn btn-lg bg-green waves-effect"]
            );
            echo $this->Html->link(
                'Volver',
                ['action' => 'index'],
                ['class' => "btn btn-lg btn-info waves-effect"]
            );
        ?>
    </div>

    <?php echo $this->Form->end() ?>

    <hr>







    <?php
        echo $this->Html->css(['/tema/plugins/bootstrap-select/css/bootstrap-select'], ['block' => 'css']);
        echo $this->Html->css(['/tema/plugins/sweetalert/sweetalert'], ['block' => 'css']);
        echo $this->Html->script(['/tema/plugins/sweetalert/sweetalert.min'], ['block' => 'script']);
        echo $this->Html->script(['/tema/plugins/jquery-validation/jquery.validate'], ['block' => 'script']);
        echo $this->Html->script(['/tema/plugins/jquery-steps/jquery.steps'], ['block' => 'script']);
        echo $this->Html->script(['/tema/js/pages/forms/form-validation'], ['block' => 'script']);
    ?>
