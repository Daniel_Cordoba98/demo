<?php
    $this->Form->setTemplates([
        // Container element used by control().
        'inputContainer' => '{{content}}',

        // Container element used by control() when a field has an error.
        'inputContainerError' => '{{content}}',

    ]);
?>

    <style>
        .bootstrap-select {
            width: 100% !important;
        }

        .btn {
            margin-right: 8px !important;
        }

        .error-message {
            color: red;
            font-size: 12px;
        }

    </style>

    <h3>Editar Usuario</h3>

    <hr>

    <?php echo $this->Form->create($user) ?>


    <div class="row clearfix">

        <div class="col-sm-3">
            <?php echo $this->Form->input('tipo_documento',
                    ['type' => 'select',
                        'options' => [
                            'CC' => 'CC',
                            'NIT' => 'NIT',
                            'TI' => 'TI',
                        ],
                        'label' => 'Tipo Documento',
                        'class' => 'show-tick',
                        'empty' => 'Tipo Documento',
                ]) ?>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('documento', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Número Documento',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('nombres', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Nombres',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('apellidos', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Apellidos',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('username', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Nombre Usuario',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-5">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('email', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'E-mail',
                    ]) ?>
                </div>

                <?php
                    if ($this->Form->isFieldError('email')) {
                        echo $this->Form->error('email');
                    }
                ?>

            </div>
        </div>

        <div class="col-sm-4">
            <?php echo $this->Form->input('rol_id',
                    ['type' => 'select',
                        'options' => $roles,
                        'class' => 'show-tick',
                        'label' => 'Rol',
                        'empty' => 'SELECCIONAR ROL',
                ]) ?>
        </div>

        <div class="col-sm-5">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('telefono', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Teléfono',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <?php echo $this->Form->input('activo', [
                    'id' => 'md_checkbox_10',
                    'type' => 'checkbox',
                    'class' => 'chk-col-light-green',
                    'label' => false,
            ]) ?>
            <label for="md_checkbox_10">Activo</label>

        </div>
    </div>

    <hr>

    <div class="row" style="margin-left: 8px;">
        <?php
            echo $this->Form->button(
                'Guardar',
                ['class' => "btn btn-lg bg-green waves-effect"]
            );
            echo $this->Html->link(
                'Volver',
                ['action' => 'index'],
                ['class' => "btn btn-lg btn-info waves-effect"]
            );
        ?>
    </div>

    <?php echo $this->Form->end() ?>



    <?php
        echo $this->Html->css(['/tema/plugins/bootstrap-select/css/bootstrap-select'], ['block' => 'css']);
    ?>
