<h3>Gestión de Usuarios</h3>
<hr>

<?php echo $this->Html->link(
        'Crear Usuario',
        ['action' => 'add'],
        ['class' => 'btn bg-light-blue btn-lg waves-effect',
    ]) ?>

<hr>


<div class="body table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">
                    <?php echo $this->Paginator->sort('id') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('tipo_documento') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('documento') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('nombres', 'Nombre Completo') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('rol_id') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('created', 'Creado') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('modified', 'Modificado') ?>
                </th>
                <th scope="col" class="actions">
                    <?php echo ('Acciones') ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td>
                    <?php echo $this->Number->format($user->id) ?>
                </td>
                <td>
                    <?php echo h($user->tipo_documento) ?>
                </td>
                <td>
                    <?php echo h($user->documento) ?>
                </td>
                <td>
                    <?php echo h($user->nombre_completo) ?>
                </td>
                <td>
                    <?php echo h($user->rol['descripcion']) ?>
                </td>
                <td>
                    <?php echo h($user->created) ?>
                </td>
                <td>
                    <?php echo h($user->modified) ?>
                </td>
                <td class="actions">

                    <?php echo $this->Html->link(
                            '<i class="material-icons col-blue">zoom_in</i>',
                            ['action' => 'view', $user->id],
                            [
                                'escape' => false,
                                'style' => 'padding-top: 6px;',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => 'Ver',
                        ]) ?>

                    <?php echo $this->Html->link(
                            '<i class="material-icons col-blue">edit</i>',
                            ['action' => 'edit', $user->id],
                            [
                                'escape' => false,
                                'style' => 'padding-top: 6px;',
                                'data-toggle' => 'tooltip',
                                'data-original-title' => 'Editar',
                        ]) ?>

                    <?php echo $this->Html->Link(
                            '<span data-toggle="tooltip"
                                 data-placement="top"
                                 style="padding-top: 6px"
                                 title="Borrar">
                                 <i class="material-icons col-blue">delete</i>
                            </span>',
                            '#',
                            [
                                'escape' => false,
                                'data-toggle' => "modal",
                                'data-target' => "#defaultModal_$user->id",

                        ]) ?>

                    <!-- Default Size -->
                    <div class="modal fade" id="defaultModal_<?php echo $user->id ?>" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="defaultModalLabel">Eliminar Usuario</h4>
                                </div>
                                <div class="modal-body">
                                    ¿Esta seguro de eliminar el usuario
                                    <strong>
                                        <?php echo $user->nombres . " " . $user->apellidos ?>
                                    </strong>?
                                </div>
                                <div class="modal-footer">
                                    <hr>
                                    <?php echo $this->Form->postLink(
                                            'ELIMINAR',
                                            ['action' => 'delete', $user->id],
                                            ['class' => 'btn btn-danger waves-effect',
                                        ]) ?>
                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?php echo $this->Paginator->first('<< ' . ('Primero')) ?>
            <?php echo $this->Paginator->prev('< ' . ('Anterior')) ?>
            <?php echo $this->Paginator->numbers() ?>
            <?php echo $this->Paginator->next(('Siguiente') . ' >') ?>
            <?php echo $this->Paginator->last(('Último') . ' >>') ?>
        </ul>
        <hr>
        <p class="font-italic col-grey">
            <?php echo $this->Paginator->counter(['format' => ('Página {{page}} de {{pages}}, Mostrando {{current}} Registro(s) de {{count}} en Total')]) ?>
        </p>
    </div>
</div>

<?php
    echo $this->Html->script(['/tema/js/pages/ui/tooltips-popovers'], ['block' => 'script']);
    echo $this->Html->script(['/tema/js/pages/ui/modals'], ['block' => 'script']);
    echo $this->Html->script(['/tema/plugins/bootstrap-notify/bootstrap-notify'], ['block' => 'script'])
?>
