<?php

    $this->Form->setTemplates([
        'inputContainer' => '{{content}}',
    ]);

?>

    <style>
        .bootstrap-select {
            width: 100% !important;
        }

        .btn {
            margin-right: 8px !important;
        }
        
        form * :not(a) {
            cursor: default !important;
        }

    </style>

    <h3>Ver Usuario</h3>

    <hr>

    <?php echo $this->Form->create($user) ?>


    <div class="row clearfix">

        <div class="col-sm-3">
            <?php echo $this->Form->input('tipo_documento',
                    ['type' => 'select',
                        'options' => [
                            'CC' => 'CC',
                            'NIT' => 'NIT',
                            'TI' => 'TI',
                        ],
                        'label' => 'Tipo Documento',
                        'class' => 'show-tick',
                        'empty' => 'Tipo Documento',
                        'disabled',
                ]) ?>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('documento', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Número Documento',
                            'disabled',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('nombres', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Nombres',
                            'disabled',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('apellidos', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Apellidos',
                            'disabled',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('username', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Nombre Usuario',
                            'disabled',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-5">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('email', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'E-mail',
                            'disabled',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <?php echo $this->Form->input('rol_id',
                    ['type' => 'select',
                        'options' => $roles,
                        'class' => 'show-tick',
                        'label' => 'Rol',
                        'empty' => 'SELECCIONAR ROL',
                        'disabled',
                ]) ?>
        </div>

        <div class="col-sm-5">
            <div class="form-group form-float">
                <div class="form-line">
                    <?php echo $this->Form->input('telefono', [
                            'type' => "text",
                            'class' => "form-control",
                            'label' => 'Teléfono',
                            'disabled',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <?php echo $this->Form->input('activo', [
                    'id' => 'basic_checkbox_3',
                    'type' => 'checkbox',
                    'label' => false,
                    'disabled',
            ]) ?>
            <label for="basic_checkbox_3">Activo</label>

        </div>
    </div>

    <hr>

    <div class="row" style="margin-left: 8px;">
        <?php
            echo $this->Html->link(
                'Volver',
                ['action' => 'index'],
                ['class' => "btn btn-lg btn-info waves-effect"]
            );
        ?>
    </div>

    <?php echo $this->Form->end() ?>

    <?php
        echo $this->Html->css(['/tema/plugins/bootstrap-select/css/bootstrap-select'], ['block' => 'css']);
    ?>
