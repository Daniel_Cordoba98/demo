<h3>Envíos de notificaciones cumpleaños</h3>
<hr>
<?php echo $this->Html->link(
        'Generar Reporte',
        ['action' => 'reporte'],
        ['class' => 'btn bg-teal btn-lg waves-effect',
    ]) ?>
<hr>

<div class="body table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">
                    <?php echo $this->Paginator->sort('nombre_completo') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('destinatario', 'Correo Corporativo') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('resultado') ?>
                </th>
                <th scope="col">
                    <?php echo $this->Paginator->sort('created', 'Fecha de envío') ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cumpleanos as $value): ?>
            <tr>
                <td>
                    <?php echo h($value->empleados['nombre_completo']) ?>
                </td>
                <td>
                    <?php echo h(strtoupper($value->destinatario)) ?>
                </td>
                <td>
                    <?php if ($value->resultado) {
                            echo "ENVIADO";
                        } else {
                            echo "NO ENVIADO";
                        }
                    ?>
                </td>
                <td>
                    <?php echo h($value->created) ?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->first('<< ' . ('Primero'));
                echo $this->Paginator->prev('< ' . ('Anterior'));
                echo $this->Paginator->numbers();
                echo $this->Paginator->next(('Siguiente') . ' >');
                echo $this->Paginator->last(('Ultimo') . ' >>')
            ?>
        </ul>
        <hr>
        <p>
            <?php echo $this->Paginator->counter(['format' => ('Página {{page}} de {{pages}}, Mostrando {{current}} Registro(s) de {{count}}')]) ?>
        </p>
    </div>
</div>
