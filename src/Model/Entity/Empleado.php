<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Empleado extends Entity
{
    // Make all fields mass assignable for now.
    protected $_accessible = ['*' => true];

    protected function _setIdentificacion($identificacion)
    {
        // elimino cualquier caracter no numerico del campo identificacion antes de guardar
        return preg_replace("/[^0-9,.]/", "", $identificacion);
    }
}

