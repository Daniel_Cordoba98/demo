<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function initialize(array $config)
    {

        // plugin para actualizar campos created_by y modified_by
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->addBehavior('Timestamp');

        $this->hasOne('rol', [
            'className' => 'Roles',
            'bindingKey' => 'rol_id',
            'foreignKey' => 'id',
        ]);

    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('tipo_documento', 'Debe diligenciar este campo')
            ->notEmpty('documento', 'Debe diligenciar este campo')
            ->notEmpty('nombres', 'Debe diligenciar este campo')
            ->notEmpty('apellidos', 'Debe diligenciar este campo')
            ->notEmpty('username', 'Debe diligenciar este campo')
            ->notEmpty('password', 'Debe diligenciar este campo')
            ->notEmpty('rol_id', 'Debe diligenciar este campo')
            ->notEmpty('email', 'Debe diligenciar este campo');

        $validator->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'Diligencie un email valido',
        ]);

        return $validator;

    }

}
