<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class CumpleanosEmailsTable extends Table
{

    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');

        $this->hasOne('empleados', [
            'className' => 'Empleados',
            'bindingKey' => 'usuario_id',
            'foreignKey' => 'id',
            'propertyName' => 'empleados'
        ]);

    }
}
