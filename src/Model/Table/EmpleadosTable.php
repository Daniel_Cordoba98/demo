<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class EmpleadosTable extends Table
{

    public function initialize(array $config)
    {

        // plugin para actualizar campos created_by y modified_by
        $this->addBehavior('Muffin/Footprint.Footprint');

        $this->setEntityClass('App\Model\Entity\Empleado');

        $this->addBehavior('Timestamp');

        $this->hasOne('clasificacion', [
            'className' => 'PlantaClasificaciones',
            'bindingKey' => 'clasificacion_id',
            'foreignKey' => 'id',
        ]);

        $this->hasOne('proyecto', [
            'className' => 'PlantaProyectos',
            'bindingKey' => 'proyecto_id',
            'foreignKey' => 'id',
        ]);

    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('tipo_identificacion', 'Debe diligenciar este campo');

        $validator->add('correo_personal', 'validFormat', [
            'rule' => 'email',
            'message' => 'Diligencie un email valido',
        ]);
        $validator->add('correo_corporativo', 'validFormat', [
            'rule' => 'email',
            'message' => 'Diligencie un email valido',
        ]);

        $validator
            ->allowEmpty('correo_personal')
            ->allowEmpty('correo_corporativo');

        return $validator;

    }

    public function beforeSave($event, $entity, $options)
    {
        $entity->nombre_completo = trim($entity->nombres . " " . $entity->apellido_paterno . " " . $entity->apellido_materno);
        return true;
    }

}
