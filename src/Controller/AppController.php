<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

// plugin para actualizar campos created_by y modified_by
use Muffin\Footprint\Auth\FootprintAwareTrait;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    // plugin para actualizar campos created_by y modified_by
    use FootprintAwareTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [

            // mensaje de autorizacion
            'authError' => __('No estas autorizado para ver esta seccion.'),

            // cargo relacion rol en el compunenete de autentificacoin
            'authenticate' => [
                'Form' => [
                    'contain' => ['rol'],
                ],
            ],

            'loginRedirect' => [
                'controller' => 'Inicio',
                'action' => 'index',
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'],
        ]);

    }

    public function beforeFilter(Event $event)
    {
        //$this->Auth->allow();
        $this->set('usuario_logueado', $this->Auth->user());
        $this->set('pagina__anterior', $this->referer());
    }

}
