<?php
namespace App\Controller;

use App\Controller\AppController;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;

/**
 * PlantaCargue Controller
 *
 *
 * @method \App\Model\Entity\PlantaCargue[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlantaCargueController extends AppController
{

    public function index()
    {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '2048M');

        if ($this->request->is(['post', 'put'])) {

            $datos_archivo = $this->request->getData('archivo');

            if ($this->validarArchivo($datos_archivo['tmp_name'])) {
                $datos = $this->leerExcel($datos_archivo['tmp_name']);
                $datos_organizados = $this->organizarInfo($datos);

                if ($this->cargarRegistros($datos_organizados)) {
                    $this->Flash->success('Cargue / Actualizacion correcto!.');
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error('El archivo no cumple con el formato especificado.');
        }
    }

    public function validarArchivo($ruta_archivo)
    {
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($ruta_archivo);
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                $hoja[] = $row;
                break;
            }
        }
        $reader->close();

        if (isset($hoja[0]) && isset($hoja[1])) {

            if (count($hoja[0]) == 47 && count($hoja[1]) == 49) {

                // normalizo strings
                $hoja[0] = array_map('trim', $hoja[0]);
                $hoja[0] = array_map('strtoupper', $hoja[0]);
                $hoja[1] = array_map('trim', $hoja[1]);
                $hoja[1] = array_map('strtoupper', $hoja[1]);

                if (
                    $hoja[0][6] == 'IDENTIFICACION' && $hoja[1][6] == 'IDENTIFICACION' &&
                    $hoja[0][7] == 'NOMBRES' && $hoja[1][7] == 'NOMBRES' &&
                    $hoja[0][8] == 'APELLIDO PATERNO' && $hoja[1][8] == 'APELLIDO PATERNO' &&
                    $hoja[0][9] == 'APELLIDO MATERNO' && $hoja[1][9] == 'APELLIDO MATERNO' &&
                    $hoja[0][37] == 'CUENTA' && $hoja[1][37] == 'CUENTA' &&
                    $hoja[0][45] == 'CONTRATO' && $hoja[1][45] == 'CONTRATO' &&
                    strpos($hoja[0][21], "100") && strpos($hoja[1][21], "100") &&
                    strpos($hoja[0][23], "30") && strpos($hoja[1][23], "30") &&
                    $hoja[1][47] == 'MOTIVO RETIRO' &&
                    $hoja[1][48] == 'MES Y AÑO REPORTE'
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    public function leerExcel($ruta_archivo)
    {
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($ruta_archivo);

        foreach ($reader->getSheetIterator() as $sheet) {

            foreach ($sheet->getRowIterator() as $row) {
                if ($sheet->getIndex() == 0) {
                    // si esta en la hoja 1 se toma como activo
                    $row['estado'] = 1;
                    $datos['altas'][] = $row;

                    // omito fila 1 de la hoja (contiene los titulos)
                    unset($datos['altas'][0]); // MEJORAR
                } else {
                    // si esta en la hoja 2 se toma como baja
                    $row['estado'] = 0;
                    $datos['bajas'][] = $row;

                    // omito fila 1 de la hoja (contiene los titulos)
                    unset($datos['bajas'][0]); // MEJORAR

                }
            }
        }
        $reader->close();

        // organizo retorno primero bajas y luego altas
        $datos2 = array_merge($datos['bajas'], $datos['altas']);
        unset($datos);

        return $datos2;
    }

    public function organizarInfo($datos)
    {
        // cargue de modelos
        $this->loadModel('PlantaProyectos');
        $proyectos = $this->PlantaProyectos->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaCategorias');
        $categorias = $this->PlantaCategorias->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaTiposSangre');
        $tipo_sangre = $this->PlantaTiposSangre->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaClasificaciones');
        $clasificaciones = $this->PlantaClasificaciones->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $acentos = [
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E',
            'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O',
            'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o',
            'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
            'Ñ' => 'N', 'ñ' => 'n',
        ];

        foreach ($datos as $key => $valor) {

            // fechas
            foreach ($valor as $key2 => $dato) {
                if (is_object($dato)) {
                    $valor[$key2] = $valor[$key2]->format('Y-m-d');
                }
            }

            $valor = array_map('trim', $valor);
            $valor = array_map('strtoupper', $valor);

            if (!isset($valor[47])) {
                $valor[47] = '';
            }

            if (!isset($valor[48])) {
                $valor[48] = '';
            }

            //normalizacion clasificaiones
            $valor[0] = strtoupper(str_replace([" "], "", $valor[0]));

            // normalizacion tipo de sangre
            $valor[36] = str_replace([" ", "´"], "", $valor[36]);
            $valor[36] = str_replace("0", "O", $valor[36]);
            $valor[36] = strtoupper($valor[36]);

            // normalizacion proyecto
            $valor[2] = str_replace(" - ", " ", $valor[2]);
            $valor[2] = strtoupper($valor[2]);

            //normalizacion categorias
            $valor[46] = strtoupper($valor[46]);

            // normalizacion correo corporativo
            $valor[16] = strtr($valor[16], $acentos);
            $valor[16] = strtoupper($valor[16]);

            // normalizacion nombres
            $valor[7] = strtolower($valor[7]);
            $valor[7] = utf8_decode($valor[7]);
            $valor[7] = strtoupper(str_replace("?", "", $valor[7]));

            // normalizacion identificacion
            $valor[6] = preg_replace("/[^0-9,.]/", "", $valor[6]);

            // normalizacion salario y bono
            $valor[21] = $valor[21] == 0 ? null : $valor[21];
            $valor[22] = $valor[22] == 0 ? null : $valor[22];
            $valor[23] = $valor[23] == 0 ? null : $valor[23];
            $valor[24] = $valor[24] == 0 ? null : $valor[24];

            /***  relaciones  ***/
            // proyecto_id
            $proyecto_id = array_search($valor[2], $proyectos);
            $proyecto_id = (array_search($valor[2], $proyectos)) ? array_search($valor[2], $proyectos) : 0;

            // clasificaion_id
            $clasificacion_id = array_search($valor[0], $clasificaciones);
            $clasificacion_id = (array_search($valor[0], $clasificaciones)) ? array_search($valor[0], $clasificaciones) : 0;

            // categoria_id
            $categoria_id = array_search($valor[46], $categorias);
            $categoria_id = (array_search($valor[46], $categorias)) ? array_search($valor[46], $categorias) : 0;

            // tipo_sangre_id
            $tipo_sangre_id = array_search($valor[36], $tipo_sangre);
            $tipo_sangre_id = (array_search($valor[36], $tipo_sangre)) ? array_search($valor[36], $tipo_sangre) : 0;

            $datos_organizados[] = [
                'clasificacion_id' => $clasificacion_id,
                'centro_de_costo' => $valor[1],
                'proyecto_id' => $proyecto_id,
                'nueva_area' => $valor[3],
                'cargo' => $valor[4],
                'reemplazo' => $valor[5],
                'tipo_identificacion' => 'CC',
                'identificacion' => $valor[6],
                'nombres' => $valor[7],
                'apellido_paterno' => $valor[8],
                'apellido_materno' => $valor[9],
                'genero' => $valor[11],
                'ciudad_trabajo' => $valor[12],
                'direccion_residencia' => $valor[13],
                'departamento' => $valor[14],
                'correo_personal' => $valor[15],
                //'correo_corporativo' => $valor[16], correo coprorativo para no modificar campo en bd
                'contacto_personal_fijo' => $valor[17],
                'contacto_personal_celular' => $valor[18],
                'fecha_nacimiento' => $valor[19],
                'ciudad_nacimiento' => $valor[20],
                'salario_total' => $valor[21],
                'salario_parcial' => $valor[22],
                'bono_mensual' => $valor[23],
                'bono_anual' => $valor[24],
                'porcentaje_riesgo' => $valor[25],
                'eps' => $valor[26],
                'fondo' => $valor[27],
                'cesantias' => $valor[28],
                'arl' => $valor[29],
                'tipo_contrato' => $valor[30],
                'fecha_ingreso' => $valor[31],
                'fecha_retiro' => $valor[32],
                'reingreso' => $valor[33],
                'estado_civil' => $valor[34],
                'num_hijos' => $valor[35],
                'tipo_sangre_id' => $tipo_sangre_id,
                'cuenta' => $valor[37],
                'carne_cliente' => $valor[38],
                'carne_ezentis' => $valor[39],
                'resultados_examenes_ingreso' => $valor[40],
                'examenes_ingreso_enfasis' => $valor[41],
                'caja_compensacion' => $valor[42],
                'herramientcliente_t_t' => $valor[43],
                'titulo_academico' => $valor[44],
                'contrato' => $valor[45],
                'categoria_id' => $categoria_id,
                'motivo_retiro' => $valor[47],
                'mes_ano_reporte' => $valor[48],
                'estado' => $valor['estado'],
            ];
        }
        unset($datos);

        // dd($datos_organizados);
        return $datos_organizados;
    }

    public function cargarRegistros($datos_organizados)
    {

        // cargue de modelo a usar
        $this->loadModel('Empleados');

        foreach ($datos_organizados as $key => $registro) {

            $consulta = $this->Empleados->findByIdentificacion($registro['identificacion'])->toArray();

            if (empty($consulta)) {
                // crear y guarda entidad
                if ($this->Empleados->save($this->Empleados->newEntity($registro))) {
                }
            } else {
                // actualizar entidad
                $actualizar_registro = $this->Empleados->get($consulta[0]->id);
                if ($this->Empleados->save($this->Empleados->patchEntity($actualizar_registro, $registro))) {
                }
            }
        }
        return true;
    }

}
