<?php
namespace App\Controller;

use App\Controller\AppController;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Cake\Filesystem\File;

/**
 * PlantaCargue Controller
 *
 *
 * @method \App\Model\Entity\PlantaCargue[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlantaCargueInicialController extends AppController
{
    public function index()
    {
        $time_start = microtime(true);

        ini_set('max_execution_time', 3600); // 1 hora
        ini_set('memory_limit', '-1');

        $archivo = 'tmp/planta_cargue_inicial.xlsx';
        $file = new File($archivo);

        if ($file->exists()) {
            if ($this->validarArchivo($archivo)) {
                $datos = $this->leerExcel($archivo);
                $datos_organizados = $this->organizarInfo($datos);

                // guarda o actualiza en la base de datos y crea correos electronicos
                if ($this->cargarRegistros($datos_organizados)) {

                    $time_end = microtime(true);
                    $time = round($time_end - $time_start, 2);
                    echo "tiempo transcurrido $time segundos";

                    exit('Proceso terminado correctamente.');
                }
            }
            exit('El archivo no cumple con el formato especificado.');
        }
        exit('archivo "tmp/planta_cargue_inicial.xlsx" no encontrado!');
    }

    public function validarArchivo($ruta_archivo)
    {
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files

        $reader->open($ruta_archivo);
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                $hoja[] = $row;
                break;
            }
        }
        $reader->close();

        if (isset($hoja[0])) {
            if (count($hoja[0]) == 43) {
                // normalizo strings
                $hoja[0] = array_map('trim', $hoja[0]);
                $hoja[0] = array_map('strtoupper', $hoja[0]);
                if (
                    $hoja[0][6] == 'IDENTIFICACION' &&
                    $hoja[0][7] == 'NOMBRES' &&
                    $hoja[0][8] == 'APELLIDO PATERNO' &&
                    $hoja[0][9] == 'APELLIDO MATERNO' &&
                    $hoja[0][33] == 'CUENTA' &&
                    $hoja[0][41] == 'CONTRATO'
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    public function leerExcel($ruta_archivo)
    {
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($ruta_archivo);

        foreach ($reader->getSheetIterator() as $sheet) {

            foreach ($sheet->getRowIterator() as $row) {
                if ($sheet->getIndex() == 0) {
                    // si esta en la hoja 1 se toma como activo
                    $row['estado'] = 1;
                    $datos[] = $row;

                }
            }
        }
        $reader->close();

        // omito fila 1 de la hoja (contiene los titulos)
        unset($datos[0]); // MEJORAR

        return $datos;
    }

    public function organizarInfo($datos)
    {
        // cargue de modelos
        $this->loadModel('PlantaProyectos');
        $proyectos = $this->PlantaProyectos->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaCategorias');
        $categorias = $this->PlantaCategorias->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaTiposSangre');
        $tipo_sangre = $this->PlantaTiposSangre->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaClasificaciones');
        $clasificaciones = $this->PlantaClasificaciones->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        foreach ($datos as $key => $valor) {

            // fechas
            foreach ($valor as $key2 => $dato) {
                if (is_object($dato)) {
                    $valor[$key2] = $valor[$key2]->format('Y-m-d');
                }
            }

            $valor = array_map('trim', $valor);
            $valor = array_map('strtoupper', $valor);

            // elimino cualquier caracter no numerico de la identificacion
            $valor[6] = preg_replace("/[^0-9,.]/", "", $valor[6]);

            if (!isset($valor[43])) {
                $valor[43] = '';
            }

            if (!isset($valor[44])) {
                $valor[44] = '';
            }

            //normalizacion clasificaiones
            $valor[0] = strtoupper(str_replace([" "], "", $valor[0]));

            // normalizacion tipo de sangre
            $valor[32] = str_replace([" ", "´"], "", $valor[32]);
            $valor[32] = str_replace("0", "O", $valor[32]);
            $valor[32] = strtoupper($valor[32]);

            // normalizacion proyecto
            $valor[2] = str_replace(" - ", " ", $valor[2]);
            $valor[2] = strtoupper($valor[2]);

            //normalizacion categorias
            $valor[42] = strtoupper($valor[42]);

            /***  relaciones  ***/
            // proyecto_id
            $proyecto_id = array_search($valor[2], $proyectos);
            $proyecto_id = (array_search($valor[2], $proyectos)) ? array_search($valor[2], $proyectos) : 0;

            // clasificaion_id
            $clasificacion_id = array_search($valor[0], $clasificaciones);
            $clasificacion_id = (array_search($valor[0], $clasificaciones)) ? array_search($valor[0], $clasificaciones) : 0;

            // categoria_id
            $categoria_id = array_search($valor[42], $categorias);
            $categoria_id = (array_search($valor[42], $categorias)) ? array_search($valor[42], $categorias) : 0;

            // tipo_sangre_id
            $tipo_sangre_id = array_search($valor[32], $tipo_sangre);
            $tipo_sangre_id = (array_search($valor[32], $tipo_sangre)) ? array_search($valor[32], $tipo_sangre) : 0;

            $datos_organizados[] = [
                'clasificacion_id' => $clasificacion_id,
                'centro_de_costo' => $valor[1],
                'proyecto_id' => $proyecto_id,
                'proyecto_desc' => $valor[2],
                'nueva_area' => $valor[3],
                'cargo' => $valor[4],
                'reemplazo' => $valor[5],
                'tipo_identificacion' => 'CC',
                'identificacion' => $valor[6],
                'nombres' => $valor[7],
                'apellido_paterno' => $valor[8],
                'apellido_materno' => $valor[9],
                'genero' => $valor[11],
                'ciudad_trabajo' => $valor[12],
                'direccion_residencia' => $valor[13],
                'departamento' => $valor[14],
                'correo_personal' => $valor[15],
                'correo_corporativo' => $valor[16],
                'contacto_personal_fijo' => $valor[17],
                'contacto_personal_celular' => $valor[18],
                'fecha_nacimiento' => $valor[19],
                'ciudad_nacimiento' => $valor[20],
                'porcentaje_riesgo' => $valor[21],
                'eps' => $valor[22],
                'fondo' => $valor[23],
                'cesantias' => $valor[24],
                'arl' => $valor[25],
                'tipo_contrato' => $valor[26],
                'fecha_ingreso' => $valor[27],
                'fecha_retiro' => $valor[28],
                'reingreso' => $valor[29],
                'estado_civil' => $valor[30],
                'num_hijos' => $valor[31],
                'tipo_sangre_id' => $tipo_sangre_id,
                'cuenta' => $valor[33],
                'carne_cliente' => $valor[34],
                'carne_ezentis' => $valor[35],
                'resultados_examenes_ingreso' => $valor[36],
                'examenes_ingreso_enfasis' => $valor[37],
                'caja_compensacion' => $valor[38],
                'herramientcliente_t_t' => $valor[39],
                'titulo_academico' => $valor[40],
                'contrato' => $valor[41],
                'categoria_id' => $categoria_id,
                'motivo_retiro' => $valor[43],
                'mes_ano_reporte' => $valor[44],
                'estado' => $valor['estado'],
            ];
        }
        unset($datos);

        // dd($datos_organizados);
        return $datos_organizados;
    }

    public function cargarRegistros($datos_organizados)
    {
        // cargue de modelo a usar
        $this->loadModel('Empleados');

        foreach ($datos_organizados as $key => $registro) {

            $consulta = $this->Empleados->findByIdentificacion($registro['identificacion'])->toArray();

            $registro['correo_corporativo'] = $this->generarUsuario($registro); // crea correo electronico

            if (empty($consulta)) {
                // crear y guarda entidad
                if ($this->Empleados->save($this->Empleados->newEntity($registro, ['validate' => false]))) {}
            } else {
                // actualizar entidad
                $actualiar_registro = $this->Empleados->get($consulta[0]->id);
                if ($this->Empleados->save($this->Empleados->patchEntity($actualiar_registro, $registro, ['validate' => false]))) {}
            }
        }
        return true;
    }

    public function generarUsuario($datos_usuario)
    {
        $acentos = [
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'ð' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o',
            'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'Ñ' => 'N', 'ñ' => 'n',
        ];

        $nombre = explode(" ", $datos_usuario["nombres"]);
        $nombre = trim($nombre[0]);
        $apellido = trim(str_replace(" ", "", $datos_usuario["apellido_paterno"]));

        $usuario = $nombre . "." . $apellido;

        $usuario = strtr($usuario, $acentos);

        $usuario = htmlentities($usuario, null, 'utf-8');
        $usuario = str_replace("&nbsp;", "", $usuario);

        if (trim($datos_usuario['proyecto_id']) == 6 || trim($datos_usuario['proyecto_id']) == 7) {
            $usuario = strtolower($usuario) . ".telefonica@col-ezentis.com";
        } else {
            $usuario = strtolower($usuario) . "@col-ezentis.com";
        }

        $admin_correos = new \App\Custom\zimbraSoapApi\zimbraSoapApi;

        // intenta crear correo
        if (@$admin_correos->crearCuenta($usuario, $datos_usuario['identificacion'], [
            'displayName' => $datos_usuario['nombres'] . " " . $datos_usuario['apellido_paterno'] . " " . $datos_usuario['apellido_materno'],
            'commonName' => $datos_usuario['nombres'],
            'givenName' => $datos_usuario['nombres'],
            'lastName' => $datos_usuario['apellido_paterno'] . " " . $datos_usuario['apellido_materno'],
            'description' =>  $datos_usuario['cargo'] . " - " . $datos_usuario['proyecto_desc'],
            'zimbraNotes' =>  $datos_usuario['identificacion'],
            'zimbraPasswordMustChange' => 'TRUE',
            //'zimbraFeatureMailEnabled' => 'TRUE',
            //'zimbraFeatureContactsEnabled' => 'TRUE',
        ])) {
            return $usuario;
        }

        // comprobacion de correo
        if ($admin_correos->comprobarCuenta($usuario)) {
            // si hay homonimo
            $explode_correo = explode('@', $usuario);
            $explode_usuario = explode('.', $explode_correo[0]);
            $telefonica = (isset($explode_usuario[2])) ? '.' . $explode_usuario[2] : '';

            if (!empty($datos_usuario['apellido_materno'])) {
                $usuario = strtolower($explode_usuario[0] . "." . $explode_usuario[1] . substr($datos_usuario['apellido_materno'], 0, 1) . $telefonica . "@" . $explode_correo[1]);
            } else {
                $usuario = strtolower($explode_usuario[0] . "." . $explode_usuario[1] . substr($datos_usuario['apellido_paterno'], 0, 1) . $telefonica . "@" . $explode_correo[1]);
            }

            // creo correo homonimo (añado primera letra del segundo apellido al usuario)
            @$admin_correos->crearCuenta($usuario, $datos_usuario['identificacion'], [
                'displayName' => $datos_usuario['nombres'] . " " . $datos_usuario['apellido_paterno'] . " " . $datos_usuario['apellido_materno'],
                'commonName' => $datos_usuario['nombres'],
                'givenName' => $datos_usuario['nombres'],
                'lastName' => $datos_usuario['apellido_paterno'] . " " . $datos_usuario['apellido_materno'],
                'description' => $datos_usuario['cargo'],
                'zimbraPasswordMustChange' => 'TRUE',
                //'zimbraFeatureMailEnabled' => 'TRUE',
                //'zimbraFeatureContactsEnabled' => 'TRUE',
            ]);

            return $usuario;

        } else {
            // no existe la cuenta (volver a intentarlo)
            @$admin_correos->crearCuenta($usuario, $datos_usuario['identificacion'], [
                'displayName' => $datos_usuario['nombres'] . " " . $datos_usuario['apellido_paterno'] . " " . $datos_usuario['apellido_materno'],
                'commonName' => $datos_usuario['nombres'],
                'givenName' => $datos_usuario['nombres'],
                'lastName' => $datos_usuario['apellido_paterno'] . " " . $datos_usuario['apellido_materno'],
                'description' => $datos_usuario['cargo'],
                'zimbraPasswordMustChange' => 'TRUE',
                //'zimbraFeatureMailEnabled' => 'TRUE',
                //'zimbraFeatureContactsEnabled' => 'TRUE',
            ]);

            return $usuario;
        }

    }

}
