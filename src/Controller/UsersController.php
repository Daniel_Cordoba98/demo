<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\NotFoundException;

class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'logout']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users, [
            'contain' => ['rol'],
        ]);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function view($id = null)
    {
        $this->loadModel('Roles');
        $roles = $this->Roles->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ]);

        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user', 'roles'));

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $this->loadModel('Roles');
        $roles = $this->Roles->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ]);

        $user = $this->Users->newEntity();
        if ($this->request->is(['post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(('Usuario creado correctamente'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(('El usuaro no ha sido creado. Intente nuevamente.'));
        }
        $this->set(compact('user', 'roles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Roles');
        $roles = $this->Roles->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ]);
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(('El usuario ha sido actualizado'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(('No se actualizó el usuario. Intente nuevamente'));
        }
        $this->set(compact('user', 'roles'));

    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(('Usuario eliminado correctamente'));
        } else {
            $this->Flash->error(('El usuario no ha sido eliminado. Intente nuevamente'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        // no cargo layout
        $this->viewBuilder()->setLayout(false);

        if (!empty($this->Auth->user())) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                // guardo ultimo inicio de sesion
                // obtener id del usuario get
                $user = $this->Users->get($user['id']);
                // dd($user);

                // cambiar el last login en la entidad
                $user->last_login = date('Y-m-d H:i:s');
                $user->dirty('modified', true);

                // actualizo la entidad
                $this->Users->save($user);

                // redireccion de login
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Usuario y/o Contraseña Incorrecto. Intente Nuevamente');
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

}
