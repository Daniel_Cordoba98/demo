<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Helper\HtmlHelper;

/**
 * AdministrarCorreo Controller
 *
 *
 * @method \App\Model\Entity\AdministrarCorreo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdministrarCorreoController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('restablecerContrasenaUsuario');
    }

    public function ldapIndex()
    {
        // vista index ldap
    }

    public function restablecerContrasena()
    {
        if (isset($this->request->params['?']['mail']) && $this->request->is(['get'])) {
            $this->request->data['correo_corporativo'] = $this->request->params['?']['mail'];
        }

        if ($this->request->is(['post', 'put'])) {

            if (strpos($this->request->getData('correo_corporativo'), '@ezentis.com.co') !== false) {
                $admin_correos = new \App\Custom\zimbraSoapApi\zimbraSoapApiEzentisComCo;
            } else {
                $admin_correos = new \App\Custom\zimbraSoapApi\zimbraSoapApiColEzentisCom;
            }

            if ($admin_correos->comprobarCuenta($this->request->getData('correo_corporativo'))) {
                $requiere_cambio = ($this->request->getData('requerir_cambio')) ? 'TRUE' : 'FALSE';
                $cambiar_contrasena = $admin_correos->cambiarContrasena($this->request->getData('correo_corporativo'), $this->request->getData('contrasena'), $requiere_cambio);
                if ($cambiar_contrasena) {
                    $admin_correos->desbloquearCuenta($this->request->getData('correo_corporativo'));
                    $this->Flash->success('Contraseña del correo ' . $this->request->getData('correo_corporativo') . ' Actualizada');
                    return $this->redirect(['action' => 'restablecerContrasena']);
                }
            }
            $this->request->data['contrasena'] = '';
            $this->Flash->error('El correo corporativo no existe');
        }
    }

    public function restablecerContrasenaUsuario()
    {
        $this->viewBuilder()->setLayout(false);
        $validaciones_ok = false;
        $this->loadModel('Empleados');
        $datos = $this->Empleados->newEntity();
        if ($this->request->is(['post', 'put'])) {

            $usuario = $this->Empleados->findByCorreoCorporativo($this->request->getData('correo_corporativo'))->first();
            if (!empty($usuario)) {

                if ($this->request->getData('identificacion') == $usuario->identificacion) {
                    if (
                        $this->request->getData('correo_personal') == $usuario->correo_personal ||
                        $this->request->getData('correo_personal') == strtolower($usuario->correo_personal)) {
                        // convertir lo que nos llegue del input en un objeto datetime
                        $explode = explode('/', $this->request->getData('fecha_nacimiento'));
                        $fecha_formulario = new \DateTime();
                        @$fecha_formulario->setDate($explode[2], $explode[1], $explode[0]);

                        if ($fecha_formulario->format('Y-m-d') == $usuario->fecha_nacimiento->format('Y-m-d')) {
                            $validaciones_ok = true;
                            $this->set(compact('datos', 'validaciones_ok'));
                            $this->getRequest()->getSession()->delete('Flash');
                            $this->Flash->success('La validación de información es correcta');
                        }
                    }
                }
                $this->Flash->error('La validación de información no fue correcta', ['key' => 'Error']);
            } else {
                $this->Flash->error('Correo corporativo no existente', ['key' => 'Error']);
            }

            // contraseñas
            if ($validaciones_ok) {

                $this->getRequest()->getSession()->delete('Flash.Error');

                if (!empty($this->request->getData('contrasena1')) && !empty($this->request->getData('contrasena2'))) {
                    if ($this->request->getData('contrasena1') === $this->request->getData('contrasena2')) {
                        $admin_correos = new \App\Custom\zimbraSoapApi\zimbraSoapApiColEzentisCom;

                        // cambio la clave
                        $cambio_pass = $admin_correos->cambiarContrasena($usuario->correo_corporativo, $this->request->getData('contrasena1'), 'FALSE');

                        if ($cambio_pass) {
                            // redirect al usuario con flash
                            $this->getRequest()->getSession()->delete('Flash');
                            $this->Flash->success('Contraseña restablecida con exito!');
                            return $this->redirect(['action' => 'restablecerContrasenaUsuario']);
                        }

                        $this->getRequest()->getSession()->delete('Flash');
                        $this->Flash->error('Ocurrio un error, contacte un administrador.');
                        return $this->redirect(['action' => 'restablecerContrasenaUsuario']);
                    } else {
                        $this->getRequest()->getSession()->delete('Flash');
                        $this->Flash->error('Las contraseñas no coinciden');
                    }
                }
            }
        }
        $this->set(compact('datos', 'validaciones_ok'));
    }

    public function getCorreosLdap()
    {

        $correos = [];

        $ldap_server = Configure::read('Zimbra.ezentis_com_co.host');
        $ldap_user = Configure::read('Zimbra.ezentis_com_co.ldapDn');
        $ldap_pass = Configure::read('Zimbra.ezentis_com_co.ldapPass');
        $conexion = ldap_connect($ldap_server);
        ldap_set_option($conexion, LDAP_OPT_PROTOCOL_VERSION, 3);
        if (@ldap_bind($conexion, $ldap_user, $ldap_pass)) {
            $busqueda = ldap_search($conexion, "ou=people,dc=ezentis,dc=com,dc=co", "(&(mail=*)(objectClass=zimbraAccount)(!(mail=admin*)))");
            $info = ldap_get_entries($conexion, $busqueda);
            ldap_close($conexion);

            for ($i = 0; $i < $info["count"]; $i++) {

                @$correos[] = [
                    $info[$i]["displayname"][0],
                    $info[$i]["mail"][0],
                    $this->generarBotonesAccionesLdap($info[$i]["mail"][0]),
                ];

            }
        }

        $ldap_server = Configure::read('Zimbra.col_ezentis_com.host');
        $ldap_user = Configure::read('Zimbra.col_ezentis_com.ldapDn');
        $ldap_pass = Configure::read('Zimbra.col_ezentis_com.ldapPass');
        $conexion = ldap_connect($ldap_server);
        ldap_set_option($conexion, LDAP_OPT_PROTOCOL_VERSION, 3);
        if (@ldap_bind($conexion, $ldap_user, $ldap_pass)) {
            $busqueda = ldap_search($conexion, "ou=people,dc=col-ezentis,dc=com", "(&(mail=*)(objectClass=zimbraAccount)(!(mail=admin*)))");
            $info = ldap_get_entries($conexion, $busqueda);
            ldap_close($conexion);

            for ($i = 0; $i < $info["count"]; $i++) {

                @$correos[] = [
                    $info[$i]["displayname"][0],
                    $info[$i]["mail"][0],
                    $this->generarBotonesAccionesLdap($info[$i]["mail"][0]),
                ];
            }
        }

        $json_data = array(
            "draw" => intval(1),
            "recordsTotal" => intval(count($correos)),
            "recordsFiltered" => intval(count($correos)),
            "data" => $correos, // total data array
        );

        echo json_encode($json_data);
        exit();
    }

    public function generarBotonesAccionesLdap($usuario)
    {
        $html = new HtmlHelper(new \Cake\View\View());

        $botones_acciones = '';

        $botones_acciones .= $html->link('<i class="material-icons">description</i>', [
            'action' => 'ldapDetalle', $usuario],
            [
                'escape' => false,
                'class' => 'btn bg-blue-grey btn-circle waves-effect waves-circle waves-float',
                'data-toggle' => 'tooltip',
                'title' => 'Información LDAP',
                'style' => 'margin-left: 6px;',
            ]);

        $botones_acciones .= $html->link('<i class="material-icons">vpn_key</i>', [
            'action' => 'restablecerContrasena', '?' => ['mail' => $usuario]],
            [
                'escape' => false,
                'class' => 'btn bg-indigo btn-circle waves-effect waves-circle waves-float',
                'data-toggle' => 'tooltip',
                'title' => 'Cambiar Contraseña',
                'style' => 'margin-left: 6px;',
            ]);

        $botones_acciones .= $html->link('<i class="material-icons">lock_open</i>', [
            'action' => 'desbloquearUsuario', $usuario],
            [
                'escape' => false,
                'class' => 'btn bg-green btn-circle waves-effect waves-circle waves-float',
                'data-toggle' => 'tooltip',
                'title' => 'Desbloquear Usuario',
                'style' => 'margin-left: 6px;',
            ]);

        $botones_acciones .= $html->link('<i class="material-icons">search</i>', [
            'action' => 'busquedaPorCorreo', $usuario],
            [
                'escape' => false,
                'class' => 'btn bg-teal btn-circle waves-effect waves-circle waves-float',
                'data-toggle' => 'tooltip',
                'title' => 'Ver en Planta',
                'style' => 'margin-left: 6px;',
            ]);

        return $botones_acciones;

    }

    public function ldapDetalle($cuenta)
    {

        if (strpos($cuenta, '@ezentis.com.co') !== false) {
            $api = \Zimbra\Admin\AdminFactory::instance('https://' . Configure::read('Zimbra.ezentis_com_co.host') . ':' . Configure::read('Zimbra.ezentis_com_co.apiPort') . '/service/admin/soap');
            $api->auth(Configure::read('Zimbra.ezentis_com_co.apiUser'), Configure::read('Zimbra.ezentis_com_co.apiPass'));
        } else {
            $api = \Zimbra\Admin\AdminFactory::instance('https://' . Configure::read('Zimbra.col_ezentis_com.host') . ':' . Configure::read('Zimbra.col_ezentis_com.apiPort') . '/service/admin/soap');
            $api->auth(Configure::read('Zimbra.col_ezentis_com.apiUser'), Configure::read('Zimbra.col_ezentis_com.apiPass'));
        }

        $account = new \Zimbra\Struct\AccountSelector(\Zimbra\Enum\AccountBy::NAME(), $cuenta);

        $accountInfo = $api->getAccount($account);

        foreach ($accountInfo->account->a as $key => $value) {

            switch ($value->n) {
                case 'givenName':
                    $tmp['givenName'] = $value->_;
                    break;
                case 'displayName':
                    $tmp['displayName'] = $value->_;
                    break;
                case 'uid':
                    $tmp['uid'] = $value->_;
                    break;
                case 'cn':
                    $tmp['cn'] = $value->_;
                    break;
                case 'zimbraNotes':
                    $tmp['zimbraNotes'] = $value->_;
                    break;
                case 'zimbraAccountStatus':
                    $tmp['zimbraAccountStatus'] = $value->_;
                    break;
                case 'zimbraCreateTimestamp':
                    $tmp['zimbraCreateTimestamp'] = $value->_;
                    break;
                case 'zimbraMailDeliveryAddress':
                    $tmp['zimbraMailDeliveryAddress'] = $value->_;
                    break;
                case 'zimbraLastLogonTimestamp':
                    $tmp['zimbraLastLogonTimestamp'] = $value->_;
                    break;
                case 'description':
                    $tmp['description'] = $value->_;
                    break;
            }
        }

        $datos = [
            'givenName' => @$tmp['givenName'],
            'displayName' => @$tmp['displayName'],
            'uid' => @$tmp['uid'],
            'cn' => @$tmp['cn'],
            'zimbraNotes' => @$tmp['zimbraNotes'],
            'zimbraAccountStatus' => @$tmp['zimbraAccountStatus'],
            'zimbraCreateTimestamp' => @$tmp['zimbraCreateTimestamp'],
            'zimbraMailDeliveryAddress' => @$tmp['zimbraMailDeliveryAddress'],
            'zimbraLastLogonTimestamp' => @$tmp['zimbraLastLogonTimestamp'],
            'description' => @$tmp['description'],
        ];

        $this->set(compact('datos'));
    }

    public function busquedaPorCorreo($correo)
    {
        $this->loadModel('Empleados');
        $consulta = $this->Empleados->findByCorreoCorporativo($correo)->first();

        if (!empty($consulta)) {
            return $this->redirect(['controller' => 'empleados', 'action' => 'view', $consulta->id]);
        }

        $this->Flash->error('Correo no registrado en planta');
        return $this->redirect($this->referer());
    }

    public function desbloquearUsuario($correo)
    {

        if (strpos($correo, '@ezentis.com.co') !== false) {
            $admin_correos = new \App\Custom\zimbraSoapApi\zimbraSoapApiEzentisComCo;
        } else {
            $admin_correos = new \App\Custom\zimbraSoapApi\zimbraSoapApiColEzentisCom;
        }

        if ($admin_correos->desbloquearCuenta($correo)) {
            $this->Flash->success('Desbloqueado correctamente');
            return $this->redirect($this->referer());
        }

        $this->Flash->error('Ocurrio un error, vuelva a intentarlo');
        return $this->redirect($this->referer());
    }

}
