<?php

namespace App\Controller;

use App\Controller\AppController;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Cake\Http\Exception\NotFoundException;

class EmpleadosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        // inicializo condicones a ninguna
        $condiciones = [];

        //transformo POST en GET
        if ($this->request->is(['post', 'put'])) {

            $fitrar = false;
            foreach ($this->request->getData() as $key => $value) {
                if (!empty($value)) {
                    $fitrar = true;
                }
            }

            if ($fitrar) {

                $filter_url['controller'] = $this->request->getParam('controller');
                $filter_url['action'] = $this->request->getParam('action');

                // Necesitamos sobrescribir la página cada vez que cambiemos los parámetros
                $filter_url['page'] = 1;

                // por cada filtro añadimos un parametro GET a la url generada
                foreach ($this->request->getData() as $name => $value) {
                    if ($value) {
                        // purgamos $value haciendo un urlencode para estar seguros (espacios y demas)
                        // $filter_url[$name] = urldecode($value);
                        $filter_url[$name] = rawurldecode($value);
                    }
                }
                // ahora que ya generamos una url con los parametros GET, redirigimos a la pagina
                return $this->redirect($filter_url);
            }

        } else {

            if ($this->request->getParam('?')) {
                // recorro todos los parametros 'named' con los filtros a aplicar
                foreach ($this->request->getParam('?') as $param_name => $value) {
                    // No aplico filtro para los parámetros 'named' predeterminados utilizados para la paginación...
                    if (!in_array($param_name, ['page', 'sort', 'direction', 'limit'])) {
                        // puede usar un switch aqui para hacer los filtros especiales como 'between' dates, 'greater than', etc...

                        switch ($param_name) {
                            case 'proyecto_id':
                                $condiciones = array_merge_recursive($condiciones, [
                                    'AND' => [
                                        ['Empleados.proyecto_id' => $value],
                                    ],
                                ]);
                                break;

                            case 'nombre_completo':
                                $condiciones = array_merge_recursive($condiciones, [
                                    'AND' => [
                                        ['Empleados.nombre_completo LIKE' => "%$value%"],

                                    ],
                                ]);
                                break;

                            case 'identificacion':
                                $condiciones = array_merge_recursive($condiciones, [
                                    'AND' => [
                                        ['Empleados.identificacion LIKE' => "%$value%"],
                                    ],
                                ]);
                                break;

                            default:
                                // default
                                break;
                        }
                        // relleno campos del formulario con los campos de la busqueda
                        $this->request->data[$param_name] = $value;
                    }
                }
                $cantidad = $this->Empleados->find('all', ['conditions' => $condiciones])->count();
                $this->Flash->success('Se Han Encontrado ' . $cantidad . ' Empleados');
            }
        }

        $this->loadModel('PlantaProyectos');
        $proyecto = $this->PlantaProyectos->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);
        $this->loadModel('PlantaClasificaciones');
        $clasificacion = $this->PlantaClasificaciones->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $planta = $this->paginate($this->Empleados, [
            'conditions' => $condiciones,
            'contain' => ['clasificacion', 'proyecto'],
            'order' => ['fecha_ingreso' => 'desc'],
        ]);

        $this->set(compact('planta', 'proyecto'));
    }

    /**
     * View method
     *
     * @param string|null $id planta id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('PlantaClasificaciones');
        $clasificacion = $this->PlantaClasificaciones->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaProyectos');
        $proyecto = $this->PlantaProyectos->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaTiposSangre');
        $tipo_sangre = $this->PlantaTiposSangre->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaCategorias');
        $categoria = $this->PlantaCategorias->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $planta = $this->Empleados->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('planta', 'clasificacion', 'proyecto', 'tipo_sangre', 'categoria'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('PlantaClasificaciones');
        $clasificacion = $this->PlantaClasificaciones->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaProyectos');
        $proyecto = $this->PlantaProyectos->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaTiposSangre');
        $tipo_sangre = $this->PlantaTiposSangre->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaCategorias');
        $categoria = $this->PlantaCategorias->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $planta = $this->Empleados->newEntity();
        if ($this->request->is('post')) {
            $planta = $this->Empleados->patchEntity($planta, $this->request->getData());
            if ($this->Empleados->save($planta)) {
                $this->Flash->success('El empleado ha sido creado.');

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('El empleado no ha sido creado. Intente nuevamente.');
        }
        $this->set(compact('planta', 'clasificacion', 'proyecto', 'tipo_sangre', 'categoria'));
    }

    /**
     * Edit method
     *
     * @param string|null $id planta id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('PlantaClasificaciones');
        $clasificacion = $this->PlantaClasificaciones->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaProyectos');
        $proyecto = $this->PlantaProyectos->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaTiposSangre');
        $tipo_sangre = $this->PlantaTiposSangre->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $this->loadModel('PlantaCategorias');
        $categoria = $this->PlantaCategorias->find('list', ['keyField' => 'id', 'valueField' => 'descripcion']);

        $planta = $this->Empleados->get($id, [
            //'contain' => [],
        ]);

        $planta->fecha_nacimiento = (empty($planta->fecha_nacimiento)) ? '' : $planta->fecha_nacimiento->format('Y-m-d');
        $planta->fecha_ingreso = (empty($planta->fecha_ingreso)) ? '' : $planta->fecha_ingreso->format('Y-m-d');
        $planta->fecha_retiro = (empty($planta->fecha_retiro)) ? '' : $planta->fecha_retiro->format('Y-m-d');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $planta = $this->Empleados->patchEntity($planta, $this->request->getData());
            if ($this->Empleados->save($planta)) {
                $this->Flash->success('El empleado ha sido actualizado.');
                return $this->redirect($this->request->data['redirect']);
            }
            $this->Flash->error('El empleado no se actualizó. Intente nuevamente');
        }
        $this->set(compact('planta', 'clasificacion', 'proyecto', 'tipo_sangre', 'categoria'));
    }

    /**
     * Delete method
     *
     * @param string|null $id planta id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $planta = $this->Empleados->get($id);
        if ($this->Empleados->delete($planta)) {
            $this->Flash->success('El empleado ha sido eliminado.');
        } else {
            $this->Flash->error('El empleado no ha sido eliminado. Intente nuevamente.');
        }

        return $this->redirect(['action' => 'index']);
    }

    public function reportes()
    {
        $planta = $this->Empleados->find('all', [
            'contain' => 'proyecto',
        ])->hydrate(false)->toArray();

        foreach ($planta as $key => $value1) {

            $planta[$key]['proyecto_id'] = $planta[$key]['proyecto']['descripcion'];
            unset($planta[$key]['proyecto']);

            $planta[$key] = array_map(function ($dato) {
                return (is_object($dato)) ? $dato->format('Y-m-d H:i:s') : $dato;
            }, $planta[$key]);
        }

        $titulos = array_keys($planta[0]);
        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $writer->openToBrowser('reporte_planta.csv');

        $writer->addRow($titulos);
        $writer->addRows($planta);

        $writer->close();
        exit();
    }
}
