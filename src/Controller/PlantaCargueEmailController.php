<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\Mailer\Email;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * PlantaCargueEmail Controller
 *
 *
 * @method \App\Model\Entity\PlantaCargueEmail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlantaCargueEmailController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
        ini_set('memory_limit', '-1');
    }

    public function index()
    {
        $archivos_adjuntos = $this->descargarAdjuntos();

        if (isset($archivos_adjuntos["asuntos"]) && isset($archivos_adjuntos["archivos"])) {

            foreach ($archivos_adjuntos["asuntos"] as $key => $value) {
                if (strpos($value, 'INDUCCION - NOTIFICACION INGRESO') !== false || strpos($value, 'INDUCCIÓN - NOTIFICACION INGRESO') !== false || strpos($value, 'INDUCCIÓN - NOTIFICACIÓN INGRESO') !== false) {
                    $adjuntos_a_procesar['archivos'][] = $archivos_adjuntos["archivos"][$key];
                } elseif (strpos($value, 'NOTIFICACION - RETIROS') !== false || strpos($value, 'NOFITICACION - RETIROS') !== false) {
                    $adjuntos_eliminar['archivos'][] = $archivos_adjuntos["archivos"][$key];
                } else {
                    $archivos_adjuntos['mensaje'] = "Omitiendo email " . "($key)" . " con asunto: " . $value;
                }
            }

            if (isset($adjuntos_eliminar["archivos"]) && isset($adjuntos_eliminar["archivos"])) {
                // metodos de eliminar
                $cedulas_eliminar = $this->generarFormatoEliminar($adjuntos_eliminar);
                $correos_eliminados = $this->eliminarCorreos($cedulas_eliminar);
                $notificacion_ti_eliminar = $this->notificarTiEliminar($correos_eliminados);
            }

            if (isset($adjuntos_a_procesar["archivos"]) && isset($adjuntos_a_procesar["archivos"])) {
                // metodos de crear (db y zimbra)
                $datos_acumulados = $this->generarFormatoCorreosNuevos($adjuntos_a_procesar);
                $datos_acumulados_correo = $this->crearCorreos($datos_acumulados);
                $notificacion_ti = $this->notificarTi($datos_acumulados_correo);
                $notificacion_lideres = $this->notificarLideres($datos_acumulados_correo);
            }

            foreach ($archivos_adjuntos['archivos'] as $key => $value) {
                unlink("tmp/$value");
            }

        }

        $log = json_encode([
            'archivos_adjuntos' => @$archivos_adjuntos,
            'datos_acumulados' => @$datos_acumulados,
            'datos_acumulados_correo' => @$datos_acumulados_correo,
            'notificacion_ti' => @$notificacion_ti,
            'notificacion_ti_eliminar' => @$notificacion_ti_eliminar,
            'notificacion_lideres' => @$notificacion_lideres,
            'correos_eliminados' => @$correos_eliminados,
        ]);

        $myfile = fopen("tmp/logs_servicio_correos/" . date("YmdHis") . "_log.json", "w") or die("Unable to open file!");
        fwrite($myfile, $log);
        fclose($myfile);

        print_r($archivos_adjuntos['mensaje']);
        exit();
    }

    public function logs()
    {
        $numero_pagina = intval(substr($this->request->here(), -1));
        $dir = new Folder('tmp/logs_servicio_correos');
        $arreglo_archivos = $dir->find('.*\.json');
        arsort($arreglo_archivos);

        $cantidad_archivos = 20;
        $paginas = explode(".", (count($arreglo_archivos) + $cantidad_archivos - 1) / $cantidad_archivos);
        $paginas = $paginas[0];
        $index = $numero_pagina * $cantidad_archivos;
        $archivos = [];
        for ($i = 0; $i < $cantidad_archivos; $i++) {
            if (isset($arreglo_archivos[$index + $i])) {
                $archivos[] = $arreglo_archivos[$index + $i];
            }
        }
        $this->set(compact('paginas', $paginas, 'archivos', $archivos));
    }

    public function logsDetalle($nombre_archivo)
    {
        $this->loadModel('PlantaProyectos');
        $proyectos[] = $this->PlantaProyectos->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $contenido_log = file_get_contents('tmp/logs_servicio_correos/' . $nombre_archivo);
        $detalle_log = json_decode($contenido_log, true);
        $this->set(compact('detalle_log', $detalle_log, 'proyectos'));
    }

    public function generarFormatoEliminar($adjuntos)
    {
        foreach ($adjuntos['archivos'] as $llave => $archivo) {

            try {
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('tmp/' . $archivo);
                $inputFileName = './tmp/' . $archivo;
                $inputFileType = IOFactory::identify($inputFileName);
                $reader = IOFactory::createReader($inputFileType);
                $spreadsheet = $reader->load($inputFileName);
                $spreadsheet->setActiveSheetIndex(0);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, false);
                // libero memoria
                $spreadsheet->disconnectWorksheets();
                $spreadsheet->garbageCollect();
                unset($spreadsheet);
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage() . '<br> Omitiendo archivo... <hr>';
                continue;
            }

            //elimino cabeceras del excel - titulos
            unset($sheetData[0]);

            // elimino arrays en blanco teniendo en cuenta nombres y apellido
            foreach ($sheetData as $key01 => $value01) {
                if (!is_null($value01[3])) {
                    $datos_formato_eliminar[$key01] = [
                        $value01[3],
                        $value01[4],
                        $value01[5],
                        $value01[6],
                        $value01[9],
                        $value01[10],
                    ];
                    //unset($sheetData[$key01]);
                }
            }
            unset($sheetData);
        }
        return $datos_formato_eliminar;
    }

    public function eliminarCorreos($cedulas)
    {
        $this->loadModel('Empleados');

        foreach ($cedulas as $key => $cedula) {

            // consultar la tabla planta y si existe la cedula no hacemos nada
            $consulta_cedula = $this->Empleados->findByIdentificacion($cedula[0])->first();

            $admin_correos_col_ezentis_com = new \App\Custom\zimbraSoapApi\zimbraSoapApiColEzentisCom;
            $admin_correos_ezentis_com_co = new \App\Custom\zimbraSoapApi\zimbraSoapApiEzentisComCo;

            if (strpos(strtolower($consulta_cedula->correo_corporativo), "col-ezentis.com")) {
                $eliminar_correo = @$admin_correos_col_ezentis_com->eliminarCuenta($consulta_cedula->correo_corporativo) ? "eliminado" : "error o no existe";
            } else {
                $eliminar_correo = @$admin_correos_ezentis_com_co->eliminarCuenta($consulta_cedula->correo_corporativo) ? "eliminado" : "error o no existe";
            }

            // actualizar entidad a estado inactivo
            $result_planta = "no existente en planta";
            if (!empty($consulta_cedula)) {
                $entity = $this->Empleados->get($consulta_cedula->id);
                $entity->fecha_retiro = date('Y-m-d', strtotime($cedula[4]));
                $entity->motivo_retiro = strtoupper($cedula[5]);
                $entity->estado = false;
                $entity->correo_corporativo = ($eliminar_correo) ? null : $entity->correo_corporativo;
                $result_planta = ($this->Empleados->save($entity, ['validate' => false])) ? "actualizado" : "error al actualizar";
            }

            $retorno[] = [
                'cedula' => $cedula[0],
                'nombres' => $cedula[1],
                'apellido1' => $cedula[2],
                'apellido2' => $cedula[3],
                'correo' => @$consulta_cedula->correo_corporativo,
                'resultado' => $eliminar_correo,
                'resultado_planta' => $result_planta,
            ];
        }
        return $retorno;
    }

    public function notificarTi($datos)
    {
        $email = new Email('default');

        $email->from(['no-responder@ezentis.com.co' => 'Gestión de correos']);

        $email->to('aplicativos@ezentis.com.co');
        $email->addTo('daniel.cordoba@ezentis.com.co');

        $email->subject('SISTEMA: Correos Creados - ' . date("Y-m-d H:i:s"));

        $email->template('correo_ti', 'gestion_correos');
        $email->emailFormat('html');

        $email->attachments('img/email/logo.jpg');

        $email->viewVars([
            'cabecera' => 'Los siguientes correos fueron creados el ' . date("Y-m-d H:i:s"),
            'datos' => $datos,
        ]);

        try {
            $envio = $email->send();
            $datos['result_envio'] = true;
        } catch (\Cake\Network\Exception\SocketException $e) {
            $datos['result_envio'] = false;
        }
        return $datos;
    }

    public function notificarTiEliminar($datos)
    {
        $email = new Email('default');

        $email->setfrom(['no-responder@ezentis.com.co' => 'Gestión de correos']);

        $email->setTo('aplicativos@ezentis.com.co');
        $email->addTo('daniel.cordoba@ezentis.com.co');

        $email->setSubject('Notificacion correos eliminados ' . date("Y-m-d H:i:s"));

        $email->setTemplate('correo_ti_eliminar', 'gestion_correos');
        $email->setLayout('gestion_correos');
        $email->emailFormat('html');

        $email->setAttachments('img/email/logo.jpg');

        $email->viewVars([
            'cabecera' => 'Los siguientes correos fueron eliminados el ' . date("Y-m-d H:i:s"),
            'datos' => $datos,
        ]);

        try {
            $envio = $email->send();
            $datos['result_envio'] = true;
        } catch (\Cake\Network\Exception\SocketException $e) {
            $datos['result_envio'] = false;
        }
        return $datos;
    }

    public function generarFormatoCorreosNuevos($adjuntos)
    {
        $this->loadModel('PlantaClasificaciones');
        $clasificacion = $this->PlantaClasificaciones->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaTiposSangre');
        $tipo_sangre = $this->PlantaTiposSangre->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        $this->loadModel('PlantaProyectos');
        $proyectos = $this->PlantaProyectos->find('list', [
            'keyField' => 'id',
            'valueField' => 'descripcion',
        ])->toArray();

        if (isset($adjuntos['archivos'])) {

            $acentos = [
                'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A',
                'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E',
                'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
                'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O',
                'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U',
                'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a',
                'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
                'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
                'ð' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o',
                'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
                'Ñ' => 'N', 'ñ' => 'n',
            ];

            foreach ($adjuntos['archivos'] as $llave => $archivo) {

                unset($sheetData, $datos_formato);

                try {

                    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('tmp/' . $archivo);
                    $inputFileName = './tmp/' . $archivo;
                    $inputFileType = IOFactory::identify($inputFileName);
                    $reader = IOFactory::createReader($inputFileType);
                    $spreadsheet = $reader->load($inputFileName);
                    $spreadsheet->setActiveSheetIndex(0);
                    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, false, false);
                    // libero memoria
                    $spreadsheet->disconnectWorksheets();
                    $spreadsheet->garbageCollect();
                    unset($spreadsheet);
                } catch (Exception $e) {
                    echo 'Error: ' . $e->getMessage() . '<br> Omitiendo archivo... <hr>';
                    continue;
                }

                // elimino arrays en blanco teniendo en cuenta nombres y apellido
                foreach ($sheetData as $key01 => $value01) {
                    if (is_null($value01[5]) || is_null($value01[6])) {
                        unset($sheetData[$key01]);
                    }
                }

                //elimino cabeceras del excel - titulos
                unset($sheetData[0]);

                // preparo arreglo para el formato
                foreach ($sheetData as $key => $value) {
                    /* formato de fecha entera a fecha string desde excel */
                    $value[11] = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($value[11], 'YYYY-MM-DD');
                    $value[12] = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($value[12], 'YYYY-MM-DD');

                    $datos_formato[$key]["nombres"] = trim($value[6]);
                    $datos_formato[$key]["apellidos"] = trim($value[7]);
                    $datos_formato[$key]["apellidos2"] = trim($value[8]);
                    $nombre1 = explode(" ", $datos_formato[$key]["nombres"]);
                    $nombre1 = trim($nombre1[0]);
                    $apellido1 = explode(" ", $datos_formato[$key]["apellidos"]);
                    $apellido1 = trim($apellido1[0]);
                    $usuario = $nombre1 . "." . $apellido1;

                    $usuario = strtr($usuario, $acentos);

                    $usuario = str_replace(array(" ", "  "), "", $usuario);

                    $usuario = htmlentities($usuario, null, 'utf-8');
                    $usuario = str_replace("&nbsp;", "", $usuario);

                    if (trim($value[1]) == "CGP" || trim($value[1]) == "CCST") {
                        $usuario = strtolower($usuario) . ".telefonica@ezentis.com.co";
                    } elseif (trim($value[1]) !== "CGP" && trim($value[1]) !== "CCST" && trim($value[0]) == "MOD") {
                        $usuario = strtolower($usuario) . "@col-ezentis.com";
                    } else {
                        $usuario = strtolower($usuario) . "@ezentis.com.co";
                    }

                    // normalizacion tipo de sangre
                    $value[13] = str_replace([" ", "´"], "", $value[13]);
                    $value[13] = str_replace("0", "O", $value[13]);
                    $value[13] = strtoupper($value[13]);

                    // normalizacion proyecto
                    $proyecto_str = $value[1];
                    $proyecto_str = strtr($proyecto_str, $acentos);
                    $proyecto_str = trim(strtoupper($proyecto_str));
                    $value[1] = str_replace(" - ", " ", $value[1]);
                    $value[1] = trim(strtoupper($value[1]));
                    $value[1] = strtr($value[1], $acentos);

                    // normalizacion clasificacion
                    $clasificacion_str = trim(strtoupper($value[0]));

                    // clasificaion_id
                    $clasificacion_id = array_search($value[0], $clasificacion);
                    $clasificacion_id = (array_search($value[0], $clasificacion)) ? array_search($value[0], $clasificacion) : 0;

                    // tipo_sangre_id
                    $tipo_sangre_id = array_search($value[13], $tipo_sangre);
                    $tipo_sangre_id = (array_search($value[13], $tipo_sangre)) ? array_search($value[13], $tipo_sangre) : 0;

                    // proyecto_id
                    $proyecto_id = array_search($value[1], $proyectos);
                    $proyecto_id = (array_search($value[1], $proyectos)) ? array_search($value[1], $proyectos) : 0;

                    $datos_formato[$key]["clasificacion_str"] = $clasificacion_str;
                    $datos_formato[$key]["clasificacion_id"] = $clasificacion_id;
                    $datos_formato[$key]["usuario"] = $usuario;
                    $datos_formato[$key]["identificacion"] = strval($value[5]);
                    $datos_formato[$key]["cargo"] = trim($value[3]);
                    $datos_formato[$key]["proyecto_str"] = $proyecto_str;
                    $datos_formato[$key]["proyecto_id"] = $proyecto_id;
                    $datos_formato[$key]["nueva_area"] = trim($value[2]);
                    $datos_formato[$key]["reemplazo"] = trim($value[4]);
                    $datos_formato[$key]["correo_personal"] = trim($value[9]);
                    $datos_formato[$key]["celular"] = trim($value[10]);
                    $datos_formato[$key]["tipo_sangre_id"] = $tipo_sangre_id;

                    try {
                        $datos_formato[$key]["fecha_nacimiento"] = (new \DateTime($value[11]))->format('Y-m-d');
                    } catch (\Exception $e) {
                        $datos_formato[$key]["fecha_nacimiento"] = null;
                    }

                    try {
                        $datos_formato[$key]["fecha_ingreso"] = (new \DateTime($value[11]))->format('Y-m-d');
                    } catch (\Exception $e) {
                        $datos_formato[$key]["fecha_ingreso"] = null;
                    }

                }

                if (!isset($datos_formato_acumulado)) {
                    $datos_formato_acumulado = $datos_formato;
                } else {
                    $datos_formato_acumulado = array_merge($datos_formato_acumulado, $datos_formato);
                }
            }

            $arreglo = [];
            foreach ($datos_formato_acumulado as $key => $value) {
                if (in_array($value['identificacion'], $arreglo)) {
                    unset($datos_formato_acumulado[$key]);
                }
                $arreglo[] = $value['identificacion'];
            }
            unset($arreglo);

            return $datos_formato_acumulado;
        }
        return false;
    }

    public function notificarLideres($datos_formato_acumulado)
    {
        // agrupo datos por proyecto para correos discriminados por proyecto
        $datos_por_proyecto = [];
        foreach ($datos_formato_acumulado as $key => $value) {
            if (!in_array($value["proyecto_str"], $datos_por_proyecto)) {
                $datos_por_proyecto[$value["proyecto_str"]][] = $value;
            } else {
                $datos_por_proyecto[$value["proyecto_str"]][] .= $value;
            }
        }

        $email = new Email('default');

        $email->from(['no-responder@ezentis.com.co' => 'Gestión de correos ' . date('Y-m-d')]);

        // preparo y envio mensajes a cada cordinador/es
        foreach ($datos_por_proyecto as $key => $value) {

            $proyecto_original = $key;
            $key = trim($key);
            $key = str_replace([" ", "-", "_"], "", $key);
            $key = strtoupper($key);
            $para = [];
            $advertencia = null;
            $concat_asunto = null;
            switch ($key) {
                case 'CGP':
                    $para = array(
                        'kevin.osorio.telefonica@ezentis.com.co',
                        'nanci.chauta.telefonica@ezentis.com.co',
                        'harol.rojas.telefonica@ezentis.com.co',
                        'angela.chacon.telefonica@ezentis.com.co',
                        'lilian.carchi.telefonica@ezentis.com.co',
                    );
                    break;
                case 'CCST':
                    $para = array('maira.quiroga.telefonica@ezentis.com.co', 'yenny.cifuentes.telefonica@ezentis.com.co');
                    break;
                case 'NOKIA':
                    $para = array('edison.santos@ezentis.com.co');
                    break;
                case 'BUCLE':
                    $para = array('manuel.cardenas@ezentis.com.co', 'julian.saa@ezentis.com.co');
                    break;
                case 'BUCLEBOGOTA':
                    $para = array('manuel.cardenas@ezentis.com.co', 'julian.saa@ezentis.com.co');
                    break;
                case 'BUCLEANTIOQUIA':
                    $para = array('william.jimenez@ezentis.com.co', 'jose.garcia@ezentis.com.co');
                    break;
                case 'BUCLECUNDINAMARCA':
                    $para = array('luis.garcia@ezentis.com.co', 'oscar.burgos@ezentis.com.co');
                    break;
                case 'BUCLECHOCO':
                    $para = array('william.jimenez@ezentis.com.co', 'jose.garcia@ezentis.com.co');
                    break;
                case 'ADMINISTRATIVO':
                    $para = array('gloria.veloza@ezentis.com.co');
                    break;
                default:
                    $advertencia = "<u><b>¡CORREOS PENDIENTES POR NOTIFICAR SIN PROYECTO!</b></u><br><BR>";
                    $concat_asunto = "SIN NOTIFICAR: ";
                    break;
            }

            $para[] = "jonathan.pedraza@ezentis.com.co";
            $para[] = "milena.tobon@ezentis.com.co";
            $para[] = "karen.delgado@ezentis.com.co";
            $para[] = "anyee.tipacoque@ezentis.com.co";
            $para[] = "daniel.cordoba@ezentis.com.co";

            $email->setTo($para);

            $email->setSubject($concat_asunto . ' Notificación correos creados ' . $proyecto_original . date(" Y-m-d H:i:s"));

            // Cuerpo del correo
            $email->setTemplate('correo_lideres');
            $email->setLayout('gestion_correos');
            $email->emailFormat('html');

            $email->setAttachments('img/email/logo.jpg');

            $email->viewVars([
                'cabecera' => 'Buen día, informamos que la(s) siguiente(s) cuenta(s) de correo corporativo han sido creadas:',
                'datos' => $value,
            ]);

            try {
                $envio = $email->send();
                $value['result_envio'] = true;
                $retorno[$proyecto_original][] = $value;
            } catch (\Cake\Network\Exception\SocketException $e) {
                $value['result_envio'] = false;
                $retorno[$proyecto_original][] = $value;
            }
        }
        return $retorno;
    }

    public function crearCorreos($datos_formato_acumulado)
    {
        // cargue de modelos necesarios
        $this->loadModel('Empleados');

        $admin_correos_col_ezentis_com = new \App\Custom\zimbraSoapApi\zimbraSoapApiColEzentisCom;
        $admin_correos_ezentis_com_co = new \App\Custom\zimbraSoapApi\zimbraSoapApiEzentisComCo;

        foreach ($datos_formato_acumulado as $key => $value) {

            // consultar la tabla planta y si existe la cedula no hacemos nada
            $consulta_cedula = $this->Empleados->findByIdentificacion($value['identificacion'])->count();

            // default de estado de correo
            $datos_formato_acumulado[$key]['creado'] = "ya existente en planta";

            if (strpos($value['usuario'], 'col-ezentis.com')) {
                $admin_correos = $admin_correos_col_ezentis_com;
            } else {
                $admin_correos = $admin_correos_ezentis_com_co;
            }

            if ($consulta_cedula == 0) {
                // si no exixte en planta

                // comprobacion de correo
                if ($admin_correos->comprobarCuenta($value['usuario'])) {
                    // si hay homonimo
                    $explode_correo = explode('@', $value['usuario']);
                    $explode_usuario = explode('.', $explode_correo[0]);
                    $telefonica = (isset($explode_usuario[2])) ? '.' . $explode_usuario[2] : '';

                    if (!empty($value['apellidos2'])) {
                        $value['usuario'] = strtolower($explode_usuario[0] . "." . $explode_usuario[1] . substr($value['apellidos2'], 0, 1) . $telefonica . "@" . $explode_correo[1]);
                        $datos_formato_acumulado[$key]['usuario'] = $value['usuario'];
                    } else {
                        $value['usuario'] = strtolower($explode_usuario[0] . "." . $explode_usuario[1] . substr($value['apellidos'], 0, 1) . $telefonica . "@" . $explode_correo[1]);
                        $datos_formato_acumulado[$key]['usuario'] = $value['usuario'];
                    }
                }

                // crear correo en servidor zimbra
                @$crear_correo = $admin_correos->crearCuenta($value['usuario'], "Ez." . $value['identificacion'], [
                    'displayName' => $value['nombres'] . " " . $value['apellidos'] . " " . $value['apellidos2'],
                    'commonName' => $value['nombres'],
                    'givenName' => $value['nombres'],
                    'lastName' => $value['apellidos'] . " " . $value['apellidos2'],
                    'description' => $value['cargo'] . " - " . $value['proyecto_str'],
                    'zimbraNotes' => $value['identificacion'],
                    'zimbraPasswordMustChange' => 'TRUE',
                ]);

                $datos_formato_acumulado[$key]['creado'] = ($crear_correo) ? "correo creado" : "error al intetnar crear";

                // guardar el registro en la tabla de planta
                $registro = $this->Empleados->newEntity();
                $registro->clasificacion_id = $value['clasificacion_id'];
                $registro->nombres = $value['nombres'];
                $registro->apellido_paterno = $value['apellidos'];
                $registro->apellido_materno = $value['apellidos2'];
                $registro->correo_corporativo = $value['usuario'];
                $registro->identificacion = $value['identificacion'];
                $registro->cargo = $value['cargo'];
                $registro->nueva_area = $value['nueva_area'];
                $registro->reemplazo = $value['reemplazo'];
                $registro->correo_personal = $value['correo_personal'];
                $registro->contacto_personal_celular = $value['celular'];
                $registro->fecha_nacimiento = $value['fecha_nacimiento'];
                $registro->fecha_ingreso = $value['fecha_ingreso'];
                $registro->tipo_sangre_id = $value['tipo_sangre_id'];
                $registro->estado = 1;
                $registro->proyecto_id = $value['proyecto_id'];

                // actulizo el arreglo con el correo nuevo
                $datos_formato_acumulado[$key]['guardado_db'] = false;
                if ($this->Empleados->save($registro, ['validate' => false])) {
                    $datos_formato_acumulado[$key]['guardado_db'] = true;
                }
                // ya existe en planta
            } else {
                $consulta_cedula = $this->Empleados->find('all', [
                    'conditions' => ['identificacion' => $value['identificacion']],
                    'contain' => 'proyecto',
                ])->first();

                if (empty($consulta_cedula->correo_corporativo)) {
                    // comprobacion de correo
                    if ($admin_correos->comprobarCuenta($value['usuario'])) {
                        // si hay homonimo
                        $explode_correo = explode('@', $value['usuario']);
                        $explode_usuario = explode('.', $explode_correo[0]);
                        $telefonica = (isset($explode_usuario[2])) ? '.' . $explode_usuario[2] : '';

                        if (!empty($value['apellidos2'])) {
                            $value['usuario'] = strtolower($explode_usuario[0] . "." . $explode_usuario[1] . substr($value['apellidos2'], 0, 1) . $telefonica . "@" . $explode_correo[1]);
                            $datos_formato_acumulado[$key]['usuario'] = $value['usuario'];
                        } else {
                            $value['usuario'] = strtolower($explode_usuario[0] . "." . $explode_usuario[1] . substr($value['apellidos'], 0, 1) . $telefonica . "@" . $explode_correo[1]);
                            $datos_formato_acumulado[$key]['usuario'] = $value['usuario'];
                        }
                    }
                    // crear correo en servidor zimbra
                    @$crear_correo = $admin_correos->crearCuenta($value['usuario'], "Ez." . $consulta_cedula->identificacion, [
                        'displayName' => $consulta_cedula->nombre_completo,
                        'commonName' => $consulta_cedula->nombres,
                        'givenName' => $consulta_cedula->nombres,
                        'lastName' => $consulta_cedula->apellido_paterno . " " . $consulta_cedula->apellido_materno,
                        'description' => $consulta_cedula->cargo . " - " . $consulta_cedula->proyecto->descripcion,
                        'zimbraNotes' => $consulta_cedula->identificacion,
                        'zimbraPasswordMustChange' => 'TRUE',
                    ]);

                    $entity = $this->Empleados->get($consulta_cedula->id);
                    $entity->correo_corporativo = $value['usuario'];
                    $entity->estado = true;
                    $this->Empleados->save($entity, ['validate' => false]);

                    $datos_formato_acumulado[$key]['creado'] = ($crear_correo) ? "ya existente en planta, correo creado" : "ya existente en planta, correo <b>NO CREADO</b>";
                    $datos_formato_acumulado[$key]['usuario'] = $value['usuario'];

                } else {

                    // crear correo en servidor zimbra
                    @$crear_correo = $admin_correos->crearCuenta(strtolower($consulta_cedula->correo_corporativo), "Ez." . $consulta_cedula->identificacion, [
                        'displayName' => $consulta_cedula->nombre_completo,
                        'commonName' => $consulta_cedula->nombres,
                        'givenName' => $consulta_cedula->nombres,
                        'lastName' => $consulta_cedula->apellido_paterno . " " . $consulta_cedula->apellido_materno,
                        'description' => $consulta_cedula->cargo . " - " . $consulta_cedula->proyecto->descripcion,
                        'zimbraNotes' => $consulta_cedula->identificacion,
                        'zimbraPasswordMustChange' => 'TRUE',
                    ]);

                    // guardar el registro en la tabla de planta
                    $entity = $this->Empleados->get($consulta_cedula->id);
                    $entity->estado = true;
                    $this->Empleados->save($entity, ['validate' => false]);

                    $datos_formato_acumulado[$key]['creado'] = ($crear_correo) ? "ya existente en planta, correo creado" : "ya existente en planta, correo <b>NO CREADO</b>";
                    $datos_formato_acumulado[$key]['usuario'] = strtolower($consulta_cedula->correo_corporativo);

                }
            }
        }
        return $datos_formato_acumulado;
    }

    public function descargarAdjuntos()
    {
        /* connect to gmail with your credentials */
        $hostname = '{mail.networkstestamerica.com:993/ssl}INBOX';
        $username = 'novedades.it@ezentis.com.co';
        $password = '3Z3nt152018';

        /* try to connect */
        $inbox = imap_open($hostname, $username, $password) or die('Ha fallado la conexión: ' . imap_last_error());

        $emails = imap_search($inbox, 'UNSEEN');

        /* if any emails found, iterate through each email */
        if ($emails) {

            $count_email = 1;

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach ($emails as $email_number) {

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                //$message = imap_fetchbody($inbox,$email_number,2);

                // Obtener asunto
                foreach ($overview as $overview2) {
                    $asunto = iconv_mime_decode($overview2->subject, 0, "UTF-8");
                }

                $retornar["asuntos"][$count_email] = $asunto;

                /* get mail structure */
                $structure = imap_fetchstructure($inbox, $email_number);

                $attachments = array();

                /* if any attachments found... */
                if (isset($structure->parts) && count($structure->parts)) {
                    for ($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => '',
                        );

                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }

                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i + 1);

                            /* 3 = BASE64 encoding */
                            if ($structure->parts[$i]->encoding == 3) {
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            }
                            /* 4 = QUOTED-PRINTABLE encoding */
                            elseif ($structure->parts[$i]->encoding == 4) {
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }

                /* iterate through each attachment and save it */
                foreach ($attachments as $attachment) {
                    if ($attachment['is_attachment'] == 1) {
                        $filename = $attachment['name'];
                        if (empty($filename)) {
                            $filename = $attachment['filename'];
                        }

                        if (empty($filename)) {
                            $filename = time() . ".dat";
                        }

                        if (substr($filename, -3) == 'xls' || substr($filename, -4) == 'xlsx') {
                            $fp = fopen("tmp/" . $email_number . "-" . $filename, "w+");
                            fwrite($fp, $attachment['attachment']);
                            fclose($fp);
                            $retornar['archivos'][$count_email] = $email_number . "-" . $filename;
                        }
                    }
                }
                $count_email++;
            }
            $retornar['mensaje'] = "Correo " . $username . " procesado completamente";
        } else {
            $retornar['mensaje'] = "No hay correos nuevos en " . $username;
        }

        /* close the connection */
        imap_close($inbox);

        return $retornar;

    }
}
