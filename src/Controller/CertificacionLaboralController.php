<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Inicio Controller
 *
 *
 * @method \App\Model\Entity\Inicio[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CertificacionLaboralController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $this->request->allowMethod(['post']);
        $this->loadModel('Empleados');
        $persona = $this->Empleados->find('all', [
            'conditions' => [
                'identificacion' => $this->request->getData('cedula'),
            ],
        ])->first();

        if (empty($persona)) {
            exit('Cedula no encontrada, por favor contacte con recursos humanos - Ezentis Colombia');
        }

        if (!$this->validarCampos($persona)) {
            if (empty($this->request->getData('intranet'))) {
                $this->Flash->error('Los campos para generar el certificado no estan completos');
                return $this->redirect(['controller' => 'empleados', 'action' => 'index']);
            } else {
                exit('La informacion para generar su certificado esta imcompleta, por favor contacte con recursos humanos.');
            }
        }

        require_once ROOT . DS . 'vendor' . DS . 'arielcr' . DS . 'numero-a-letras' . DS . 'src' . DS . 'NumeroALetras.php';

        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'download' => true, // This can be omitted if "filename" is specified.
                 'filename' => 'certificado_' . $persona->identificacion . '.pdf', // This can be omitted if you want file name based on URL.
            ],
        ]);

        $this->set(compact('persona'));
    }

    public function validarCampos($persona)
    {
        if (!empty($persona->nombre_completo)) {
            if (!empty($persona->identificacion)) {
                if (!empty($persona->tipo_contrato)) {
                    if (!empty($persona->fecha_ingreso)) {
                        if (!empty($persona->cargo)) {
                            if (!empty($persona->bono_anual) && !empty($persona->salario_parcial)) {
                                if (!$persona->estado) {
                                    return (!empty($persona->fecha_retiro)) ? true : false;
                                } else {
                                    return true;
                                }
                            }
                            if (!empty($persona->salario_total) && empty($persona->bono_anual) && empty($persona->salario_parcial)) {
                                if (!$persona->estado) {
                                    return (!empty($persona->fecha_retiro)) ? true : false;
                                } else {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

}
