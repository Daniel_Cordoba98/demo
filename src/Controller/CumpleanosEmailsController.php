<?php
namespace App\Controller;

use App\Controller\AppController;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Cake\Mailer\Email;

/**
 * Cumpleanos Controller
 *
 *
 * @method \App\Model\Entity\Cumpleano[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CumpleanosEmailsController extends AppController
{
    public function index()
    {
        $this->loadModel('Empleados');
        $this->loadModel('CumpleanosEmails');

        $cumpleanos = $this->paginate($this->CumpleanosEmails, [
            'contain' => ['empleados'],
            'order' => ['created' => 'desc'],
        ]);

        $this->set(compact('cumpleanos'));
    }

    public function notificacionCumpleanos()
    {
        $this->loadModel('Empleados');
        $this->loadModel('CumpleanosEmails');
        // Busca empleados que cumplen años
        $fecha_nacimiento = (new \DateTime)->format('m-d');
        $cumpleanos = $this->Empleados->find('all', [
            'conditions' => [
                'Empleados.fecha_nacimiento LIKE' => "%$fecha_nacimiento%",
                'Empleados.estado ' => true,
            ],
            'fields' => ['id', 'nombres', 'fecha_nacimiento', 'correo_corporativo'],
        ]);

        //notifica empleados
        foreach ($cumpleanos as $key => $value) {

            if (!empty($value->correo_corporativo)) {

                // configuracion y envio de email
                $email = new Email('mailtrap');
                $email->setFrom(['aplicativos@ezentis.com.co' => 'Notificación cumpleaños']);
                $email->setTo(strtolower($value->correo_corporativo));
                $email->subject('¡Feliz cumpleaños ' . ucwords(strtolower($value->nombres)) . ' te desea Ezentis Colombia!');
                $email->setTemplate('correo_cumpleanos');
                $email->setLayout('gestion_correos');
                $email->emailFormat('html');
                $email->attachments('img/email/logo.jpg');

                $email->viewVars([
                    'cabecera' => 'Hola ' . ucwords(strtolower($value->nombres)) . ', Gracias por formar parte de la familia Ezentis, deseamos que tengas un feliz cumpleaños.',
                ]);

                try {
                    $envio = $email->send();
                    $datos['resultado'] = true;
                } catch (\Cake\Network\Exception\SocketException $e) {
                    $datos['resultado'] = false;
                }

                // guardo registro
                $registro = $this->CumpleanosEmails->newEntity();

                $registro->usuario_id = $value->id;
                $registro->destinatario = $value->correo_corporativo;
                $registro->resultado = $datos['resultado'];
                
                $this->CumpleanosEmails->save($registro);
            }
        }

        // return $datos;

        exit();
    }

    public function reporte()
    {

        $this->loadModel('Empleados');

        $cumpleanos = $this->CumpleanosEmails->find('all', [
            'contain' => 'empleados',
            //'limit' => 10,
        ])->hydrate(false)->toArray();

        foreach ($cumpleanos as $key => $value1) {

            unset($cumpleanos[$key]['empleados']['id']);
            $cumpleanos[$key] = array_merge($cumpleanos[$key], $cumpleanos[$key]['empleados']);
            unset($cumpleanos[$key]['empleados']);

            $cumpleanos[$key] = array_map(function ($dato) {
                return (is_object($dato)) ? $dato->format('Y/m/d') : $dato;
            }, $cumpleanos[$key]);


            unset(
                $cumpleanos[$key]['usuario_id']
            );

        }

        // dd($cumpleanos);

        $writer = WriterFactory::create(Type::CSV);
        $writer->setFieldDelimiter(';');
        $writer->openToBrowser('reporte_cumpleanos.csv');

        $writer->addRows($cumpleanos);

        $writer->close();

        exit();
    }
}
