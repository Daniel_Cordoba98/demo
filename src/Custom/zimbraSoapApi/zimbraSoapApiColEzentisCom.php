<?php

namespace App\Custom\zimbraSoapApi;

use Cake\Core\Configure;

class zimbraSoapApiColEzentisCom
{
    /* PROPIEDADES */
    public $config;
    public $apiAdmin;

    /* METODOS */
    public function __construct()
    {
        $this->config = [
            'SOAP_URL' => 'https://' . Configure::read('Zimbra.col_ezentis_com.host') . ':' . Configure::read('Zimbra.col_ezentis_com.apiPort') . '/service/admin/soap',
            'SOAP_USER' => Configure::read('Zimbra.col_ezentis_com.apiUser'),
            'SOAP_PASS' => Configure::read('Zimbra.col_ezentis_com.apiPass'),
        ];
        
        $this->apiAdmin = \Zimbra\Admin\AdminFactory::instance($this->config["SOAP_URL"]);

        try {
            $this->apiAdmin->auth($this->config["SOAP_USER"], $this->config["SOAP_PASS"]);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            exit("Imposible conectar con Api Soap, compruebe la url o las credenciales<br>" . $e->getMessage());
        }
    }

    public function idCuenta($usuario)
    {
        $account = new \Zimbra\Struct\AccountSelector(\Zimbra\Enum\AccountBy::NAME(), $usuario);
        try {
            $info = $this->apiAdmin->getAccountInfo($account);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return false;
        }
        return $info->a[0]->_;
    }

    public function cambiarContrasena($usuario, $contrasena, $debeCambiarPass = 'TRUE')
    {
        $cuentaID = $this->idCuenta($usuario);

        if (!$cuentaID) {
            return false;
        }

        $zimbraPasswordMustChange = new \Zimbra\Struct\KeyValuePair('zimbraPasswordMustChange', $debeCambiarPass);

        try {
            $this->apiAdmin->setPassword($cuentaID, $contrasena);
            $this->apiAdmin->modifyAccount($cuentaID, [$zimbraPasswordMustChange]);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function comprobarCuenta($usuario)
    {
        if (!$this->idCuenta($usuario)) {
            return false;
        }
        return true;
    }

    public function eliminarCuenta($usuario)
    {
        $cuentaID = $this->idCuenta($usuario);

        if (!$cuentaID) {
            return false;
        }

        try {
            $this->apiAdmin->deleteAccount($cuentaID);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function desbloquearCuenta($usuario)
    {
        $cuentaID = $this->idCuenta($usuario);

        if (!$cuentaID) {
            return false;
        }

        try {
            $zimbraAccountStatus = new \Zimbra\Struct\KeyValuePair('zimbraAccountStatus', 'active');
            $this->apiAdmin->modifyAccount($cuentaID, [$zimbraAccountStatus]);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function crearCuenta($usuario, $contrasena, $atributos = array(
        'displayName' => '',
        'commonName' => '',
        'givenName' => '',
        'lastName' => '',
        'description' => '',
        'zimbraNotes' => '',
        'zimbraPasswordMustChange' => 'TRUE',
        'zimbraFeatureMailEnabled' => 'TRUE',
        'zimbraFeatureContactsEnabled' => 'TRUE',
    )) {
        // genero atributos
        $displayName = new \Zimbra\Struct\KeyValuePair('displayName', $atributos['displayName']);
        $commonName = new \Zimbra\Struct\KeyValuePair('cn', $atributos['commonName']);
        $givenName = new \Zimbra\Struct\KeyValuePair('givenName', $atributos['givenName']);
        $lastName = new \Zimbra\Struct\KeyValuePair('sn', $atributos['lastName']);
        $description = new \Zimbra\Struct\KeyValuePair('description', $atributos['description']);
        $zimbraNotes = new \Zimbra\Struct\KeyValuePair('zimbraNotes', $atributos['zimbraNotes']);
        $zimbraPasswordMustChange = new \Zimbra\Struct\KeyValuePair('zimbraPasswordMustChange', $atributos['zimbraPasswordMustChange']);
        $zimbraFeatureMailEnabled = new \Zimbra\Struct\KeyValuePair('zimbraFeatureMailEnabled', $atributos['zimbraFeatureMailEnabled']);
        $zimbraFeatureContactsEnabled = new \Zimbra\Struct\KeyValuePair('zimbraFeatureContactsEnabled', $atributos['zimbraFeatureContactsEnabled']);
        $zimbraPasswordLockoutEnabled = new \Zimbra\Struct\KeyValuePair('zimbraPasswordLockoutEnabled', 'FALSE');
        
        try {
            // creo la cuenta
            $this->apiAdmin->CreateAccount($usuario, $contrasena, [
                $displayName,
                $commonName,
                $givenName,
                $lastName,
                $description,
                $zimbraNotes,
                $zimbraPasswordMustChange,
                $zimbraFeatureMailEnabled,
                $zimbraFeatureContactsEnabled,
                $zimbraPasswordLockoutEnabled,
            ]);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            return false;
        }
        return true;
    }

}
