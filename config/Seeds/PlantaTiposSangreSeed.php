<?php
use Migrations\AbstractSeed;

/**
 * PlantaTiposSangre seed.
 */
class PlantaTiposSangreSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'descripcion' => 'O+',
            ],
            [
                'id' => 2,
                'descripcion' => 'O-',
            ],
            [
                'id' => 3,
                'descripcion' => 'A+',
            ],
            [
                'id' => 4,
                'descripcion' => 'A-',
            ],
            [
                'id' => 5,
                'descripcion' => 'AB+',
            ],
            [
                'id' => 6,
                'descripcion' => 'AB-',
            ],
            [
                'id' => 7,
                'descripcion' => 'B+',
            ],
            [
                'id' => 8,
                'descripcion' => 'B-',
            ],
        ];

        $table = $this->table('planta_tipos_sangre');
        $table->insert($data)->save();
    }
}
