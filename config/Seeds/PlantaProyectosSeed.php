<?php
use Migrations\AbstractSeed;

/**
 * PlantaProyectos seed.
 */
class PlantaProyectosSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'descripcion' => 'BUCLE',
            ],
            [
                'id' => '2',
                'descripcion' => 'BUCLE ANTIOQUIA',
            ],
            [
                'id' => '3',
                'descripcion' => 'BUCLE BOGOTA',
            ],
            [
                'id' => '4',
                'descripcion' => 'BUCLE CHOCO',
            ],
            [
                'id' => '5',
                'descripcion' => 'BUCLE CUNDINAMARCA',
            ],
            [
                'id' => '6',
                'descripcion' => 'CCST',
            ],
            [
                'id' => '7',
                'descripcion' => 'CGP',
            ],
            [
                'id' => '8',
                'descripcion' => 'CONTROL PERDIDAS',
            ],
            [
                'id' => '9',
                'descripcion' => 'CUADRILLAS CODENSA',
            ],
            [
                'id' => '10',
                'descripcion' => 'ESINCO',
            ],
            [
                'id' => '11',
                'descripcion' => 'GERENCIA DESARROLLO DE NEGOCIOS',
            ],
            [
                'id' => '12',
                'descripcion' => 'GERENCIA ENERGIA',
            ],
            [
                'id' => '13',
                'descripcion' => 'GERENCIA FINANCIERA',
            ],
            [
                'id' => '14',
                'descripcion' => 'GERENCIA GENERAL',
            ],
            [
                'id' => '15',
                'descripcion' => 'GERENCIA RECURSOS',
            ],
            [
                'id' => '16',
                'descripcion' => 'GERENCIA TECNOLOGIA',
            ],
            [
                'id' => '17',
                'descripcion' => 'GERENCIA TELECOMUNICACIONES',
            ],
            [
                'id' => '18',
                'descripcion' => 'NOKIA',
            ],
            [
                'id' => '19',
                'descripcion' => 'PLAN CALIDAD',
            ],
        ];

        $table = $this->table('planta_proyectos');
        $table->insert($data)->save();
    }
}
