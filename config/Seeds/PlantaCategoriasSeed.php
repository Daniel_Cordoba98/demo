<?php
use Migrations\AbstractSeed;

/**
 * PlantaCategorias seed.
 */
class PlantaCategoriasSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'descripcion' => 'DIRECTIVO',
            ],
            [
                'id' => '2',
                'descripcion' => 'PERSONAL DE ESTRUCTURA',
            ],
            [
                'id' => '3',
                'descripcion' => 'PERSONAL DE OBRA',
            ],
            [
                'id' => '4',
                'descripcion' => 'MANDO INTERMEDIO',
            ],
        ];

        $table = $this->table('planta_categorias');
        $table->insert($data)->save();
    }
}
