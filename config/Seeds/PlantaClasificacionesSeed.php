<?php
use Migrations\AbstractSeed;

/**
 * PlantaClasificaciones seed.
 */
class PlantaClasificacionesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'descripcion' => 'MOD',
            ],
            [
                'id' => '2',
                'descripcion' => 'MOE',
            ],
            [
                'id' => '3',
                'descripcion' => 'MOI',
            ],
        ];

        $table = $this->table('planta_clasificaciones');
        $table->insert($data)->save();
    }
}
