<?php
use Migrations\AbstractMigration;

class AlterHerramientCliente extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empleados');
        $table->renameColumn('herramientcliente_t-t', 'herramientcliente_t_t');
        $table->update();
    }
}
