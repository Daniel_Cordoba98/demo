<?php
use Migrations\AbstractMigration;

class RenameUserCreatedAndModifiedAllTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {

        $table = $this->table('empleados');
        $table->renameColumn('user_created', 'created_by');
        $table->renameColumn('user_modified', 'modified_by');
        $table->update();

        $table = $this->table('users');
        $table->addColumn('created_by', 'datetime', ['null' => true]);
        $table->addColumn('modified_by', 'datetime', ['null' => true]);
        $table->update();
    }

    public function down()
    {
        $table = $this->table('empleados');
        $table->renameColumn('created_by', 'user_created');
        $table->renameColumn('modified_by', 'user_modified');
        $table->update();

        $table = $this->table('users');
        $table->removeColumn('created_by');
        $table->removeColumn('modified_by');
        $table->update();
    }

}
