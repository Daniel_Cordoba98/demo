<?php
use Migrations\AbstractMigration;

class AddColumnNombreCompleto extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empleados');
        $table->addColumn('nombre_completo', 'string', ['after' => 'apellido_materno']);
        $table->update();

    }
}
