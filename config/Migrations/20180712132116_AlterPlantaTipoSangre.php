<?php
use Migrations\AbstractMigration;

class AlterPlantaTipoSangre extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empleados');
        $table->renameColumn('tipo_sangre', 'tipo_sangre_id');
        $table->update();
    }
}
