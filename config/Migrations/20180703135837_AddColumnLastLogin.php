<?php
use Migrations\AbstractMigration;

class AddColumnLastLogin extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('last_login', 'datetime', ['after' => 'telefono']);
        $table->update();

    }
}
