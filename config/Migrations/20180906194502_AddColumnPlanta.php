<?php
use Migrations\AbstractMigration;

class AddColumnPlanta extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empleados');
        $table->addColumn('salario_total', 'string', ['after' => 'ciudad_nacimiento']);
        $table->addColumn('salario_parcial', 'string', ['after' => 'salario_total']);
        $table->addColumn('bono_mensual', 'string', ['after' => 'salario_parcial']);
        $table->addColumn('bono_anual', 'string', ['after' => 'bono_mensual']);
        $table->update();
        
    }
}
