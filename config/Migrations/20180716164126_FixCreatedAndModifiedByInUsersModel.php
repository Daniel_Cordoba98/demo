<?php
use Migrations\AbstractMigration;

class FixCreatedAndModifiedByInUsersModel extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->changeColumn('created_by', 'integer', ['null' => true]);
        $table->changeColumn('modified_by', 'integer', ['null' => true]);
        $table->update();
    }

        public function down()
    {
        $table = $this->table('users');
        $table->changeColumn('created_by', 'datetime', ['null' => true]);
        $table->changeColumn('modified_by', 'datetime', ['null' => true]);
        $table->update();
    }
}
