<?php
use Migrations\AbstractMigration;

class AlterPlantaIdentificacion extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empleados');
        $table->addIndex(['identificacion'], ['unique' => true]);
        $table->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('empleados');
        $table->addIndex(['identificacion'], ['unique' => false]);
        $table->save();
    }
}
