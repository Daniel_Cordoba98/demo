<?php
use Migrations\AbstractMigration;

class ModifiedTablePlanta extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empleados');
        $table->changeColumn('salario_total', 'string', ['null' => true]);
        $table->changeColumn('salario_parcial', 'string', ['null' => true]);
        $table->changeColumn('bono_mensual', 'string', ['null' => true]);
        $table->changeColumn('bono_anual', 'string', ['null' => true]);
        $table->update();

    }
    public function down()
    {
        $table = $this->table('empleados');
        $table->changeColumn('salario_total', 'string', ['null' => true]);
        $table->changeColumn('salario_parcial', 'string', ['null' => true]);
        $table->changeColumn('bono_mensual', 'string', ['null' => true]);
        $table->changeColumn('bono_anual', 'string', ['null' => true]);
        $table->update();

    }
}
