<?php
use Migrations\AbstractMigration;

class CreatePlantaTipoSangre extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('planta_tipos_sangre');
        $table->addColumn('descripcion', 'string');
        $table->create();
    }
}
