<?php
use Migrations\AbstractMigration;

class AlterClasificacionEzentis extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('empleados');
        $table->renameColumn('clasificacion_ezentis_id', 'clasificacion_id');
        $table->update();
    }
}
